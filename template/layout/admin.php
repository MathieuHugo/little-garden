<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Little Garden</title>
    <link rel="icon" type="image/x-icon" href="../../public/asset/img/logo.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <?php if($view->isDevMode()) {echo $view->add_webpack_style('debug');} ?>
    <?php echo $view->add_webpack_style('app'); ?>
    <?php echo $view->add_webpack_style('admin'); ?>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
</head>
<body id="page-top">
<div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
        <li><a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= $view->path('home'); ?>">
                <div class="sidebar-brand-icon">
                    <i class="fa-solid fa-tree"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Little Garden</div>
            </a>
        </li>
        <li><hr class="sidebar-divider my-0"></li>
        <li class="nav-item active">
            <a class="nav-link" href="<?= $view->path('admin'); ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
        <li><hr class="sidebar-divider"></li>
        <li>
            <div class="sidebar-heading">
                Gestion : User
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= $view->path('admin-parent'); ?>">
                <i class="fa-solid fa-user"></i>
                <span>Parent</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= $view->path('admin-pro'); ?>">
                <i class="fa-solid fa-user-tie"></i>
                <span>Professionnel</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?= $view->path('Deconnexion'); ?>">
                <i class="fa-solid fa-user-tie"></i>
                <span>Deconnexion</span></a>
        </li>
    </ul>

    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>
                <ul class="navbar-nav ml-auto">
                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">Douglas McGee</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                             aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                Profile
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                Settings
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                Activity Log
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </nav>
            <div class="container">
                <?= $content; ?>
            </div>
        </div>
    </div>
</div>

<?php
if($view->isDevMode()) {
    echo '<div id="debug-bar" class="debug-bar"></div>';
    echo $view->add_webpack_script('debug');
}
?>
<?php echo $view->add_webpack_script('app'); ?>
<?php echo $view->add_webpack_script('admin'); ?>
</body>
</html>
