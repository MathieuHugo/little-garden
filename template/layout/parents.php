<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Enriqueta&family=Oswald:wght@600&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <script src="https://js.stripe.com/v3/"></script>
    <title>Little Garden</title>
    <?php if($view->isDevMode()) {echo $view->add_webpack_style('debug');} ?>
    <?php echo $view->add_webpack_style('parent'); ?>
    <?php echo $view->add_webpack_style('familles'); ?>
</head>
<body>
<?php // $view->dump($view->getFlash()) ?>
<header id="masthead-par">
    <div class="navigationes">
        <div class="gogo">
            <div class="logogo">
                <img src="<?= $view->asset('img/logo.png'); ?>" alt="logo">
            </div>
            <p>Little Garden</p>
        </div>
        <nav>
            <ul>
                <li><a href="<?= $view->path('AccueilParents'); ?>">Accueil</a></li>
                <li><a href="<?= $view->path('MesReservations'); ?>">Rdv</a></li>
                <li><a href="<?= $view->path('MesEnfants'); ?>">Enfants</a></li>
                <li><a href="<?= $view->path('familles'); ?>">Responsable</a></li>
                <li><a href="<?= $view->path('ModifierMonProfil',array('id'=>$_SESSION['usersoid555564546Tuydy6']['id'])); ?>">Profil</a></li>
                <li><a href="<?= $view->path('Deconnexion'); ?>"><i class="fa-solid fa-user-slash"></i> Déconnexion</a></li>
            </ul>
        </nav>
    </div>
</header>

<header id="mini-bar">
    <div class="mini">
        <div class="dash" onclick="location.href='<?php echo $view->path('AccueilParents'); ?>'">
            <div class="imgo">
                <img src="<?= $view->asset('img/home.svg'); ?>" alt="icon">
            </div>
            <div class="desc">
                <p>Accueil</p>
            </div>
        </div>
        <div class="dash" onclick="location.href='<?php echo $view->path('familles'); ?>'">
            <div class="imgo">
                <img src="<?= $view->asset('img/nounou.svg'); ?>" alt="icon">
            </div>
            <div class="desc">
                <p>Responsable</p>
            </div>

        </div>
        <div class="dash" onclick="location.href='<?php echo $view->path('MesReservations'); ?>'">
            <div class="imgo">
                <img src="<?= $view->asset('img/calendar.svg'); ?>" alt="icon">
            </div>
            <div class="desc">
                <p>RDV</p>
            </div>
        </div>
        <div class="dash" onclick="location.href='<?php echo $view->path('MesEnfants'); ?>'">
            <div class="imgo">
                <img src="<?= $view->asset('img/family.svg'); ?>" alt="icon">
            </div>
            <div class="desc">
                <p>Kids</p>
            </div>
        </div>
        <div class="dash" onclick="location.href='<?php echo $view->path('ModifierMonProfil',array('id'=>$_SESSION['usersoid555564546Tuydy6']['id'])); ?>'">
            <div class="imgo">
                <img src="<?= $view->asset('img/compte.svg'); ?>" alt="icon">
            </div>
            <img class="alert" src="<?= $view->asset('img/alert.svg'); ?>" alt="icon">
            <div class="desc">
                <p>Compte</p>
            </div>
        </div>
        <div class="dash" onclick="location.href='<?php echo $view->path('Deconnexion'); ?>'">
            <div class="imgo">
                <img src="<?= $view->asset('img/compte.svg'); ?>" alt="icon">
            </div>
            <img class="alert" src="<?= $view->asset('img/alert.svg'); ?>" alt="icon">
            <div class="desc">
                <p>Déconnexion</p>
            </div>
        </div>
    </div>
</header>

<div class="container">
    <?= $content; ?>
</div>

<?php
if($view->isDevMode()) {
    echo '<div id="debug-bar" class="debug-bar"></div>';
    echo $view->add_webpack_script('debug');
}
?>
<?php echo $view->add_webpack_script('parent');
if($view->isCurrentPage('ReservationNounou') || $view->isCurrentPage('ModifierMaReservation')){
    echo $view->add_webpack_script('reservation');
}
?>
<?php
if($view->isCurrentPage('familles')) {
    echo $view->add_webpack_script('famille');
}
?>

</body>
</html>