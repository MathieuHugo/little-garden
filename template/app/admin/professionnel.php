<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?= $title; ?></h1>
</div>

<table class="table-fill">
    <thead>
    <tr>
        <th class="text-left" scope="col">ID</th>
        <th class="text-left" scope="col">Nom</th>
        <th class="text-left" scope="col">Prénom</th>
        <th class="text-left" scope="col">Email</th>
        <th class="text-left" scope="col">Vérifier</th>
        <th class="text-left" scope="col">Valider</th>
    </tr>
    </thead>
    <tbody class="table-hover">
    <?php foreach ($pros as $pro) { ?>
        <tr>
            <td class="text-left"><?= $pro->id ?></td>
            <td class="text-left"><?= $pro->nom ?></td>
            <td class="text-left"><?= $pro->prenom ?></td>
            <td class="text-left"><?= $pro->email ?></td>
            <td class="text-left" onclick="location.href='<?php echo $view->path('SingleNounou',array('id'=>$pro->id)) ?>';" style="cursor:pointer;">Voir infos</td>
            <td class="text-left">
                <a href="<?php echo $view->path('statut-pro',array('id'=>$pro->id)) ?>"><?php echo $pro->statut ?></a>
            </td>

        </tr>
    <?php } ?>
    </tbody>
</table>
