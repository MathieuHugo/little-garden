<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?= $title; ?></h1>
</div>

<table class="table-fill">
    <thead>
    <tr>
        <th class="text-left" scope="col">ID</th>
        <th class="text-left" scope="col">Nom</th>
        <th class="text-left" scope="col">Prenom</th>
        <th class="text-left" scope="col">Email</th>
    </tr>
    </thead>
    <tbody class="table-hover">
    <?php foreach ($parents as $parent) { ?>
        <tr>
            <td class="text-left"><?= $parent->id ?></td>
            <td class="text-left"><?= $parent->nom ?></td>
            <td class="text-left"><?= $parent->prenom ?></td>
            <td class="text-left"><?= $parent->email ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
