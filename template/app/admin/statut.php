<div class="wrap2">
    <div class="titre">
        <h2>Modification du statut</h2>
    </div>
<form  id='formulaire' action="" class="add" method="post" novalidate>
    <?php $listeStatut = ['actif'=>'Actif','inactif'=>'Inactif'] ?>
    <?php echo $form->label('statut', 'Statut') ?>
    <?php echo $form->select('statut',$listeStatut, $pro->statut ?? '') ?>
    <?php echo $form->error('statut') ?>

    <?php echo $form->submit('submitted', 'Modifier') ?>
</form>
</div>