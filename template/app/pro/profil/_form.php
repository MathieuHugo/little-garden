<form id="formulaire"  action="" method="post" novalidate enctype="multipart/form-data">
    <div class="bloc">
        <?= $profil->label('nom') ?>
        <?= $profil->input('nom','text',null,'Nom') ?>
        <?= $profil->error('nom') ?>

        <?= $profil->label('prenom') ?>
        <?= $profil->input('prenom','text',null,'Prenom') ?>
        <?= $profil->error('prenom') ?>

        <?= $profil->label('adresse') ?>
        <?= $profil->input('adresse','text',null,'Adresse') ?>
        <?= $profil->error('adresse') ?>

        <?= $profil->label('code_postal','code postal') ?>
        <?= $profil->input('code_postal','number',null,'Code postal') ?>
        <?= $profil->error('code_postal') ?>

        <?= $profil->label('ville') ?>
        <?= $profil->input('ville','text',null,'Ville') ?>
        <?= $profil->error('ville') ?>

        <?= $profil->label('tel') ?>
        <?= $profil->input('tel','number',null,'Télephone') ?>
        <?= $profil->error('tel') ?>

        <?= $profil->label('siren') ?>
        <?= $profil->input('siren','number',null,'Siren') ?>
        <?= $profil->error('siren') ?>

        <?= $profil->label('siret') ?>
        <?= $profil->input('siret','number',null,'Siret') ?>
        <?= $profil->error('siret') ?>

        <?= $profil->label('tarif','tarif (€/h)') ?>
        <?= $profil->input('tarif','number',null,'Tarif') ?>
        <?= $profil->error('tarif') ?>

        <?= $profil->label('max_enfant','nombre enfant max') ?>
        <?= $profil->input('max_enfant','number',null,'Max enfant') ?>
        <?= $profil->error('max_enfant') ?>

        <div class="checkbox">
            <?php foreach ($competence as $c){
                echo '<div class="one_checkbox">';
                echo '<label for="'.$c->competence.'">'.$c->competence.'</label>';
                echo '<input type="checkbox" id="'.$c->competence.'" name="competence[]" value="'.$c->id.'">';
                echo '</div>';
            } ?>
        </div>

        <?= $profil->label('image') ?>
        <?= $profil->file('image') ?>
        <?= $profil->error('image') ?><br>

        <?= $profil->label('pdf') ?>
        <?= $profil->file('pdf') ?>
        <?= $profil->error('pdf') ?><br>
    </div>
    <?= $profil->submit('submitted','Ajouter') ?>
</form>