<div class="wrap2">
    <div class="titre">
        <h1>Modifier les informations de votre enfant</h1>
    </div>
    <form  id="formulaire" action="" method="post" novalidate>
        <div class="bloc">
            <?php
            echo $form->label('nom','Son nom de famille');
            echo $form->input('nom','text',$enfant->getNom());
            echo $form->error('nom');

            echo $form->label('prenom','Son prenom');
            echo $form->input('prenom','text',$enfant->getPrenom());
            echo $form->error('prenom');

            echo $form->label('naissance','Sa date de naissance');
            echo $form->input('naissance','date',$enfant->getNaissance());
            echo $form->error('naissance');

            echo $form->label('allergie','Ses allergies');
            echo $form->textarea('allergie',$enfant->getAllergie());
            echo $form->error('allergie');

            echo $form->label('alimentation','Son alimentation');
            echo $form->textarea('alimentation',$enfant->getAlimentation());
            echo $form->error('alimentation');
            ?>
        </div>
        <?php
        echo $form->submit('submitted','Modifier')
        ?>
    </form>
</div>