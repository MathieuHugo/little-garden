<div class="wrap">
    <div class="titre">
        <h1>Mes enfants</h1>
    </div>
    <div class="ajouterResponse">
        <p><a href="<?php echo $view->path('InscrireMonEnfant') ?>"><i class="fa-solid fa-plus"></i></a></p>
    </div>
    <div id="listeEnfant">
        <?php foreach($enfants as $enfant){
            $age = date('Y') - date('Y', strtotime($enfant->getNaissance()));
            if (date('md') < date('md', strtotime($enfant->getNaissance()))) {
                $age -= 1;
            }
            ?>
            <div class="enfant">
                <div class="name">
                    <h3><?php echo $enfant->getPrenom() ?></>
                    <h3><?php echo strtoupper($enfant->getNom()) ?></>
                </div>
                <div class="age">
                    <p>Âge : <?php echo $age. ' ans'?></p>
                </div>
                <div class="picto">
                    <ul>
                        <li><a href="<?php echo $view->path('ModifierMonEnfant',array('id'=>$enfant->getId())) ?>"><i class="fa-solid fa-gears"></i></a></li>
                        <li><a onclick="return confirm('Voulez-vous effacer cet enfant ?')" href = "<?php echo $view->path('SupprimerMonEnfant',array('id'=>$enfant->getId())) ?>" ><i class="fa-solid fa-trash-can"></i></a></li>
                    </ul>
                </div>
                <div class="boutton">
                    <p><a href="<?php echo $view->path('SingleEnfant',array('id'=>$enfant->getId())) ?>">Voir le profil en détail</a></p>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="ajouter">
        <p><a href="<?php echo $view->path('InscrireMonEnfant') ?>"><i class="fa-solid fa-plus"></i></a></p>
    </div>
</div>