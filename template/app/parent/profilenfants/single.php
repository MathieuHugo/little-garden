
<div class="wrap2">
    <div id="singleEnfant">
        <?php
        $age = date('Y') - date('Y', strtotime($enfant->getNaissance()));
        if (date('md') < date('md', strtotime($enfant->getNaissance()))) {
            $age -= 1;
        }
        $jour=['Monday'=>'Lundi','Tuesday'=>'Mardi','Wednesday'=>'Mercredi','Thursday'=>'Jeudi','Friday'=>'Vendredi','Saturday'=>'Samedi','Sunday'=>'Dimanche'];
        foreach ($jour as $key=>$value){
            if(date('l',strtotime($enfant->getNaissance()))==$key){
                $jourfr=$value;
            }
        }
        $mois = ['January'=>'Janvier','February'=>'Février','March'=>'Mars','April'=>'Avril','May'=>'Mai','June'=>'Juin','July'=>'Juillet','August'=>'Août','September'=>'Septembre','October'=>'Octobre','November'=>'Novembre','December'=>"Decembre"];
        foreach ($mois as $key=>$value){
            if(date('F',strtotime($enfant->getNaissance()))==$key){
                $moisfr=$value;
            }
        }
        ?>
        <h1><?php echo $enfant->getPrenom() .' '. $enfant->getNom() ?></h1>
        <div class="naissance">
            <p> Je suis née le <?php echo strtolower($jourfr).' '.date('d',strtotime($enfant->getNaissance())) .' '. strtolower($moisfr) .' '.date('Y',strtotime($enfant->getNaissance()))?> et j'ai <?php echo $age ?> ans.</p>
        </div>
        <div class="allergie">
            <h2>Mes allergies :</h2>
            <div class="bloc">
                <p><?php echo $enfant->getAllergie() ?></p>
            </div>
        </div>
        <div class="alimentation">
            <h2>Mon alimentation :</h2>
            <div class="bloc">
                <p><?php echo $enfant->getAlimentation() ?></p>
            </div>
        </div>
    </div>
    <div class="actionSingle">
        <ul>
            <li class="modif"><a href="<?php echo $view->path('ModifierMonEnfant',array('id'=>$enfant->getId())) ?>">Modifier les informations</a></li>
            <li class="sup"><a onclick="return confirm('Voulez-vous effacer cet enfant ?')" href = "<?php echo $view->path('SupprimerMonEnfant',array('id'=>$enfant->getId())) ?>">Supprimer les informations</a></li>
        </ul>
    </div>
</div>