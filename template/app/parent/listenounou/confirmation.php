<?php
$jour=['Monday'=>'Lundi','Tuesday'=>'Mardi','Wednesday'=>'Mercredi','Thursday'=>'Jeudi','Friday'=>'Vendredi','Saturday'=>'Samedi','Sunday'=>'Dimanche'];
$mois = ['January'=>'Janvier','February'=>'Février','March'=>'Mars','April'=>'Avril','May'=>'Mai','June'=>'Juin','July'=>'Juillet','August'=>'Août','September'=>'Septembre','October'=>'Octobre','November'=>'Novembre','December'=>"Decembre"];
foreach ($jour as $key=>$value){
    if(date('l',strtotime($infoReserv->getDateStart()))==$key){
        $jourfr=$value;
    }
}
foreach ($mois as $key=>$value){
    if(date('F',strtotime($infoReserv->getDateStart()))==$key){
        $moisfr=$value;
    }
}
?>
<div class="wrap">
    <div id="confirmation">
        <div class="infoNounou">
            <h3>Ma nounou - <?php echo ucfirst($infoReserv->prenom) .' '. strtoupper($infoReserv->nom) ?></h3>
            <div class="info">
                <p><i class="fa-solid fa-location-dot"></i> <?php echo $infoReserv->adresse .' '. $infoReserv->code_postale .' '. $infoReserv->ville ?></p>
                <p><i class="fa-solid fa-phone"></i> <?php echo $infoReserv->telephone ?></p>
            </div>
        </div>
        <div class="infoReservation">
            <h3>1.Ma réservation</h3>
            <div class="bloc">
                <div class="date">
                    <strong>Enfant : <?php echo $infoReserv->prenom_enfant.' '.$infoReserv->nom_enfant?> </strong>
                    <ul>
                        <li>Date : <?php echo $jourfr .' '. date('d',strtotime($infoReserv->getDateStart())) .' '. $moisfr .' '. date('Y',strtotime($infoReserv->getDateStart())) ?></li>
                        <li>Horaire : <?php echo date('H:i',strtotime($infoReserv->getDateStart())) .' - '.  date('H:i',strtotime(date('H:i', strtotime($infoReserv->getDateStart())).'+'.$infoReserv->getNbreHeure().' hours'))?> </li>
                    </ul>
                </div>
                <div class="action">
                    <p><a href="<?php echo $view->path('ModifierMaReservation',array('id'=>$infoReserv->id_res)) ?>">Modifier</a></p>
                </div>
            </div>
        </div>
        <div class="infoTarif">
            <h3>2.Le paiement</h3>
            <div class="bloc">
                <ul>
                    <li>Nombre d'heure : <?php echo $infoReserv->getNbreHeure()?>h</li>
                    <li>Tarif : <?php echo $infoReserv->tarif ?>€/h</li>
                </ul>
                <strong>Total : <?php echo $infoReserv->getNbreHeure()*$infoReserv->tarif ?>€</strong>
            </div>
        </div>
        <div class="boutton">
            <form action="" method="post" novalidate>
                <ul class="annuler">
                    <li class="annuler"><a href="<?php echo $view->path('SupprimerMaReservation',array('id'=>$infoReserv->id_res)) ?>">Annuler</a></li>
                    <li class="confirme"><?php echo $form->submit('submitted','Confirmé') ?></li>
                </ul>
            </form>
        </div>
    </div>
</div>
