<h3 class="titru">Trouvez une nounou près de chez vous</h3>

<form id="formulaire12" action="" method="post" novalidate>
    <?= $form->label('ville'); ?>
    <?= $form->input('ville'); ?>
    <?= $form->error('ville'); ?>

    <?= $form->submit(); ?>
</form>


<section id="listingPro">
    <div class="wrapListing">
        <?php
        foreach ($donnee as $d) {?>
            <div class="onePro" onclick="location.href='<?php echo $view->path('SingleNounou',array('id'=>$d->id)) ?>';" style="cursor:pointer;">
                <div class="img">
                    <img src="<?= $view->path('upload/2024/'.$d->image) ?>" alt="">
                </div>
                <div class="infosPro">
                    <h1><?= strtoupper($d->nom) . ' ' . ucfirst($d->prenom) ?></h1>
                    <p class="role"><?= $d->role ?> à <?= $d->ville ?></p>
                   <p class="present">Bienvenue sur mon profil ! Je m'appelle Julie, et je suis ravie de vous proposer mes services en tant que..</p>
                </div>
            </div>

        <?php } ?>
    </div>
</section>