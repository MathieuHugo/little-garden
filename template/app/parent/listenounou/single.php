<section id="single_nounou">
    <div class="wrap1">
        <div class="infoPrincipal">
            <div class="imgoo">
                <img src="<?= $view->path('upload/2024/'.$pro->image) ?>" alt="">
            </div>
            <div class="inform">
                <div class="nom">
                    <h2><?= $user->nom .' '. $user->prenom ?></h2>
                </div>
                <div class="ville">
                    <span class="role"><?php if ($user->role== 'nanny'){echo 'professionnel' .' à '. $user->ville;}?></span>
                </div>
                <div class="comp">
                   <?php foreach ($competence as $c){echo '<p>'. $c->competence .'</p>'; } ?>
                </div>
            </div>
        </div>
        <div class="content-bottom">
            <div class="content">
               <p>Bienvenue sur mon profil ! <br> <br>
                   En tant que nounou expérimentée et dévouée, je suis enthousiaste à l'idée de partager ma passion pour la garde d'enfants. Mon engagement se centre sur le développement et le bien-être des tout-petits, en assurant un environnement sécurisant et enrichissant. Je m'attache à créer une atmosphère chaleureuse où l'écoute, la patience et la créativité sont les piliers de chaque journée. Convaincue que chaque enfant est unique, j'adapte mon approche pour valoriser ses intérêts et son développement personnel. Avec des jeux, des activités éducatives et des découvertes en extérieur, je veille à ce que chaque jour soit une aventure sûre et épanouissante.
               </p>
            </div>

            <div class="bottom-right">
                    <div class="adr">
                        <img src="<?= $view->asset('img/location.svg'); ?>" alt="logo">
                        <p><?= $user->adresse ?></p>
                    </div>
                    <div class="adr">
                        <img src="<?= $view->asset('img/mail.svg'); ?>" alt="logo">
                        <p><?= $user->email ?></p>
                    </div>
                    <div class="adr">
                        <img src="<?= $view->asset('img/tel.svg'); ?>" alt="logo">
                        <p><?= $user->telephone ?></p>
                    </div>
                    <div class="adr">
                        <img src="<?= $view->asset('img/nounouterie.svg'); ?>" alt="logo">
                        <p><?= $pro->getMaxEnfant() ?></p>
                        <img id="infp" src="<?= $view->asset('img/infos.svg'); ?>" alt="">
                        <div id="popup" style="display: none;">
                            Nombre d'enfants maximum
                        </div>
                    </div>


                    <div class="adr">
                        <img src="<?= $view->asset('img/payment.svg'); ?>" alt="logo">
                        <p><?= $pro->tarif ?> €/h par enfant</p>
                    </div>
                    <div class="adr">
                        <button class="button" onclick="location.href='<?php echo $view->path('ReservationNounou',array('id'=>$pro->id)) ?>';" style="cursor:pointer;">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" viewBox="0 0 24 24" height="24" fill="none" class="svg-icon"><g stroke-width="2" stroke-linecap="round" stroke="#fff"><rect y="5" x="4" width="16" rx="2" height="16"></rect><path d="m8 3v4"></path><path d="m16 3v4"></path><path d="m4 11h16"></path></g></svg>
                            <span class="lable">Réserver</span>
                        </button>
                    </div>
            </div>




            </div>

        </div>

</section>


















<script>
    function togglePopup() {
        let popup = document.getElementById("popup");
        if (popup.style.display === "none") {
            popup.style.display = "block";
        } else {
            popup.style.display = "none";
        }
    }


    document.getElementById("infp").addEventListener("click", function() {
        togglePopup();
    });
</script>

