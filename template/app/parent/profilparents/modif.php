<div class="wrap2">
    <div class="titre">
        <h2>Modifier mes informations personnelles</h2>
    </div>
    <form id="formulaire" action="" method="post" novalidate class="form">
        <div class="bloc">
            <?php
            echo $form->label('prenom');
            echo $form->input('prenom','text',$users->prenom ?? '');
            echo $form->error('prenom');

            echo $form->label('nom');
            echo $form->input('nom','text',$users->nom ?? '');
            echo $form->error('nom');

            echo $form->label('tel','telephone');
            echo $form->input('tel','number',$users->telephone ?? '');
            echo $form->error('tel');

            echo $form->label('adresse') ;
            echo $form->input('adresse','text',$users->adresse ?? '') ;
            echo $form->error('adresse');

            echo $form->label('code_postal');
            echo $form->input('code_postal','text',$users->getCodePostale() ?? '');
            echo $form->error('code_postal');

            echo $form->label('ville');
            echo $form->input('ville','text',$users->ville ?? '');
            echo $form->error('ville');?>
        </div>
        <?php echo $form->submit('submitted', 'Envoyer'); ?>

    </form>
</div>