
<div class="index">
    <div class="love">
        <h3>Trouver votre confidente de garde, là où confiance rime avec enfance !</h3>
        <h1>Prenez une réservation</h1>
        <a href="<?php echo $view->path('ListeNounou/all/') ?>" class="button">
            <svg class="svgIcon" viewBox="0 0 512 512" height="1em" xmlns="http://www.w3.org/2000/svg"><path d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zm50.7-186.9L162.4 380.6c-19.4 7.5-38.5-11.6-31-31l55.5-144.3c3.3-8.5 9.9-15.1 18.4-18.4l144.3-55.5c19.4-7.5 38.5 11.6 31 31L325.1 306.7c-3.2 8.5-9.9 15.1-18.4 18.4zM288 256a32 32 0 1 0 -64 0 32 32 0 1 0 64 0z"></path></svg>
            Rechercher
        </a>


    </div>
    <div class="infos">
        <img src="<?= $view->asset('img/kids.png'); ?>" alt="logo">
    </div>
</div>
<div class="res">
    <h1>Vos Réservations</h1>
</div>
<div class="appointments-container">
    <?php
    foreach ($infoReserves as $infoReserve){
        $jour=['Monday'=>'Lundi','Tuesday'=>'Mardi','Wednesday'=>'Mercredi','Thursday'=>'Jeudi','Friday'=>'Vendredi','Saturday'=>'Samedi','Sunday'=>'Dimanche'];
        $mois = ['January'=>'Janvier','February'=>'Février','March'=>'Mars','April'=>'Avril','May'=>'Mai','June'=>'Juin','July'=>'Juillet','August'=>'Août','September'=>'Septembre','October'=>'Octobre','November'=>'Novembre','December'=>"Decembre"];
        foreach ($jour as $key=>$value){
            if(date('l',strtotime($infoReserve->getDateStart()))==$key){
                $jourfr=$value;
            }
        }
        foreach ($mois as $key=>$value){
            if(date('F',strtotime($infoReserve->getDateStart()))==$key){
                $moisfr=$value;
            }
        }
        if(!empty($infoReserve->prix_totale)){
        ?>
            <div class="appointment-card">
                <div class="appointment-header">
                    <span class="appointment-day"><?php echo $jourfr.' '.date('d',strtotime($infoReserve->getDateStart())).' '. $moisfr ?></span>
                    <span class="appointment-time"><?php echo date('H:i',strtotime($infoReserve->getDateStart())) ?></span>
                </div>
                <div class="appointment-info">
                    <img class="profile-pic" src="<?= $view->asset('img/fame.jpg'); ?>" alt="logo">
                    <div>
                        <div class="appointment-name">Mme <?php echo $infoReserve->prenom.' '.$infoReserve->nom?></div>
                        <div class="appointment-title">Prix total : <?php echo $infoReserve->prix_totale ?>€</div>
                    </div>
                </div>
                <div class="appointment-footer">
                    <?php echo $infoReserve->prenom_enfant.' '.$infoReserve->nom_enfant ?>
                </div>
            </div>
        <?php
        }
    } ?>

</div>

<div class="card">
    <div class="card-image-container">
        <img src="<?= $view->asset('img/amour.png'); ?>" alt="" class="card-image">
    </div>
    <div class="card-content">
        <h2>Un blog reservé aux parents</h2>
        <p>Nos meilleurs conseils pour les nouveaux parents</p>
        <a href="" class="card-button">Rejoindre</a>
    </div>
</div>

<div class="card2">
    <div class="card-image-container2">
        <img src="<?= $view->asset('img/love.png'); ?>" alt="" class="card-image2">
    </div>
    <div class="card-content2">
        <h2>Vous voulez nous contacter ?</h2>
        <p>Notre équipe est disponible 24h/24</p>
        <a href="" class="card-button2">Nous contacter</a>
    </div>
</div>
