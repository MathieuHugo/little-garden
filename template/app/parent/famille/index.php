<div class="wrap">
    <div class="titreFamille">
        <h1>Famille</h1>
    </div>
    <div class="ajouterResponse">
        <p><a href="<?php echo $view->path('add-famille') ;?>"><i class="fa-solid fa-plus"></i></a></p>
    </div>
    <section id="listeFamille">
        <div class="wrapListing">
            <?php
            foreach ($familles as $f) {?>
                <div class="newFamily">
                    <div class="name">
                        <h3><?php if ($f){ echo strtoupper($f->nom) . ' ' . ucfirst($f->prenom);} ?></h3>
                        <p class="telephone"><?= $f->telephone ?></p>
                    </div>
                    <div class="picto">
                        <ul>
                            <li><a href="<?php echo $view->path('edit-famille',array('id'=>$f->getId())) ?>"><i class="fa-solid fa-gears"></i></a></li>
                            <li><a onclick="return confirm('Voulez-vous effacer cet accompagnateur ?')" href = "<?php echo $view->path('delete-famille',array('id'=>$f->getId())) ?>" ><i class="fa-solid fa-trash-can"></i></a></li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="ajouter">
            <p><a href="<?php echo $view->path('add-famille') ?>"><i class="fa-solid fa-plus"></i></a></p>
        </div>
    </section>
</div>
