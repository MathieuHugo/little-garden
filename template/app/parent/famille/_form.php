<form id="formulaire" action="" method="post" novalidate class="wrap2">
    <h1>Ajouter un accompagnateur</h1>

    <div class="bloc">
        <?= $form->label('nom'); ?>
        <?= $form->input('nom', 'text', $famille->nom ?? ''); ?>
        <?= $form->error('nom'); ?>

        <?= $form->label('prenom'); ?>
        <?= $form->input('prenom', 'text', $famille->prenom ?? ''); ?>
        <?= $form->error('prenom'); ?>

        <?= $form->label('telephone'); ?>
        <?= $form->input('telephone', 'text', $famille->telephone ?? ''); ?>
        <?= $form->error('telephone'); ?>


        <?= $form->submit('submitted', $btnText); ?>
    </div>
</form>