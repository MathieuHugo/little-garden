
<div class="wrap2">
    <div id="singleFamille">
        <h1><?php echo $famille->getPrenom() .' '. $famille->getNom() ?></h1>
        <div class="telephone">
            <p><?php echo $famille->getTelephone() ?></p>
        </div>
    </div>
    <div class="actionSingle">
        <ul>
            <li class="edit-famille"><a href="<?php echo $view->path('edit-famille',array('id'=>$famille->getId())) ?>">Modifier les informations</a></li>
            <li class="delete-famille"><a onclick="return confirm('Voulez-vous effacer ce membre ?')" href = "<?php echo $view->path('delete-damille',array('id'=>$famille->getId())) ?>">Supprimer les informations</a></li>
        </ul>
    </div>
</div>