<?php
$jour=['Monday'=>'Lundi','Tuesday'=>'Mardi','Wednesday'=>'Mercredi','Thursday'=>'Jeudi','Friday'=>'Vendredi','Saturday'=>'Samedi','Sunday'=>'Dimanche'];
$mois = ['January'=>'Janvier','February'=>'Février','March'=>'Mars','April'=>'Avril','May'=>'Mai','June'=>'Juin','July'=>'Juillet','August'=>'Août','September'=>'Septembre','October'=>'Octobre','November'=>'Novembre','December'=>"Decembre"];
?>
<div class="wrap">
    <h1>Modifier ma réservation</h1>
    <form id="formulaireReservation" action="" method="post" novalidate>
        <div class="titre">
            <h2>1. Choix du temps de garde de l'enfant </h2>
        </div>
        <div class="select">
            <?php
            $heures =['1'=>'1 heure','2'=>'2 heures','3'=>'3 heures','4'=>'4 heures','5'=>'5 heures','6'=>'6 heures','7'=>'7 heures','8'=>'8 heures','9'=>'9 heures','10'=>'10 heures','11'=>'11 heures'];
            echo $form->select('nbreheures',$heures);
            ?>
        </div>
        <div class="titre">
            <h2>2. Choix de l'horaire </h2>
        </div>
        <div class="select">
            <?php
            $listes=[];
            foreach ($enfants as $enfant){
                $listes[$enfant->getId()]=$enfant->getPrenom().' '.$enfant->getNom();
            }

            echo $form->select('enfants',$listes);
            ?>
        </div>
        <div class="titre">
            <h2>3. Choix de l'horaire </h2>
        </div>
        <div class="planning">
            <div class="flex">
                <?php for($i=1;$i<=7;$i++){
                    foreach ($jour as $key=>$value){
                        if(date('l',strtotime('+'.$i.' days'))==$key){
                            $jourfr=$value;
                        }
                    }
                    foreach ($mois as $key=>$value){
                        if(date('F',strtotime('+'.$i.' days'))==$key){
                            $moisfr=$value;
                        }
                    }
                    ?>
                    <div class="colonne">
                        <div class="date">
                            <span><?php echo strtolower($jourfr) ?></span>
                            <span><?php echo date('d',strtotime('+'.$i.' days')) .' '. strtolower($moisfr) ?></span>
                        </div>
                        <?php if(date('l',strtotime('+'.$i.' days'))=='Sunday'){
                            echo '';
                        }elseif(date('l',strtotime('+'.$i.' days'))=='Monday'){ ?>
                            <div class="horaire">
                                <?php
                                $compteur = 0;
                                for($y = idate('H',strtotime($planning->lundi_start)); $y<=idate('H',strtotime($planning->lundi_end));$y++){ ?>
                                    <div class="heure">
                                        <?php
                                        $nbre_heure = idate('H',strtotime($planning->lundi_end))-idate('H',strtotime($planning->lundi_start))+1;
                                        echo $form->inputRadio('heure',$y.':00-'.$i ,strval($nbre_heure-$compteur),'radio',date('y-m-d H:i',strtotime('+'.$i.' days '.date('H:i', strtotime($planning->lundi_start)).'+'.$compteur.' hours')));
                                        echo $form->label($y.':00-'.$i,date('H:i',strtotime($y.':00')));
                                        $compteur+=1;
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php }elseif (date('l',strtotime('+'.$i.' days'))=='Tuesday'){?>
                            <div class="horaire">
                                <?php
                                $compteur = 0;
                                for($y = idate('H',strtotime($planning->mardi_start)); $y<=idate('H',strtotime($planning->mardi_end));$y++){ ?>
                                    <div class="heure">
                                        <?php
                                        $nbre_heure = idate('H',strtotime($planning->mardi_end))-idate('H',strtotime($planning->mardi_start))+1;
                                        echo $form->inputRadio('heure',$y.':00-'.$i ,strval($nbre_heure-$compteur),'radio',date('y-m-d H:i',strtotime('+'.$i.' days '.date('H:i', strtotime($planning->mardi_start)).'+'.$compteur.' hours')));
                                        echo $form->label($y.':00-'.$i,date('H:i',strtotime($y.':00')));
                                        $compteur+=1;
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php }elseif (date('l',strtotime('+'.$i.' days'))=='Wednesday'){?>
                            <div class="horaire">
                                <?php
                                $compteur = 0;
                                for($y = idate('H',strtotime($planning->mercredi_start)); $y<=idate('H',strtotime($planning->mercredi_end));$y++){ ?>
                                    <div class="heure">
                                        <?php
                                        $nbre_heure = idate('H',strtotime($planning->mercredi_end))-idate('H',strtotime($planning->mercredi_start))+1;
                                        echo $form->inputRadio('heure',$y.':00-'.$i ,strval($nbre_heure-$compteur),'radio',date('y-m-d H:i',strtotime('+'.$i.' days '.date('H:i', strtotime($planning->mercredi_start)).'+'.$compteur.' hours')));
                                        echo $form->label($y.':00-'.$i,date('H:i',strtotime($y.':00')));
                                        $compteur+=1;
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php }elseif (date('l',strtotime('+'.$i.' days'))=='Thursday'){?>
                            <div class="horaire">
                                <?php
                                $compteur = 0;
                                for($y = idate('H',strtotime($planning->jeudi_start)); $y<=idate('H',strtotime($planning->jeudi_end));$y++){ ?>
                                    <div class="heure">
                                        <?php
                                        $nbre_heure = idate('H',strtotime($planning->jeudi_end))-idate('H',strtotime($planning->jeudi_start))+1;
                                        echo $form->inputRadio('heure',$y.':00-'.$i ,strval($nbre_heure-$compteur),'radio',date('y-m-d H:i',strtotime('+'.$i.' days '.date('H:i', strtotime($planning->jeudi_start)).'+'.$compteur.' hours')));
                                        echo $form->label($y.':00-'.$i,date('H:i',strtotime($y.':00')));
                                        $compteur+=1;
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php }elseif (date('l',strtotime('+'.$i.' days'))=='Friday'){?>
                            <div class="horaire">
                                <?php
                                $compteur = 0;
                                for($y = idate('H',strtotime($planning->vendredi_start)); $y<=idate('H',strtotime($planning->vendredi_end));$y++){ ?>
                                    <div class="heure">
                                        <?php
                                        $nbre_heure = idate('H',strtotime($planning->vendredi_end))-idate('H',strtotime($planning->vendredi_start))+1;
                                        echo $form->inputRadio('heure',$y.':00-'.$i ,strval($nbre_heure-$compteur),'radio',date('y-m-d H:i',strtotime('+'.$i.' days '.date('H:i', strtotime($planning->vendredi_start)).'+'.$compteur.' hours')));
                                        echo $form->label($y.':00-'.$i,date('H:i',strtotime($y.':00')));
                                        $compteur+=1;
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php }elseif (date('l',strtotime('+'.$i.' days'))=='Saturday'){?>
                            <div class="horaire">
                                <?php
                                $compteur = 0;
                                for($y = idate('H',strtotime($planning->samedi_start)); $y<=idate('H',strtotime($planning->samedi_end));$y++){ ?>
                                    <div class="heure">
                                        <?php
                                        $nbre_heure = idate('H',strtotime($planning->samedi_end))-idate('H',strtotime($planning->samedi_start))+1;
                                        echo $form->inputRadio('heure',$y.':00-'.$i ,strval($nbre_heure-$compteur),'radio',date('y-m-d H:i',strtotime('+'.$i.' days '.date('H:i', strtotime($planning->samedi_start)).'+'.$compteur.' hours')));
                                        echo $form->label($y.':00-'.$i,date('H:i',strtotime($y.':00')));
                                        $compteur+=1;
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
            <div class="response">
                <?php for($i=1;$i<=7;$i++){
                    foreach ($jour as $key=>$value){
                        if(date('l',strtotime('+'.$i.' days'))==$key){
                            $jourfr=$value;
                        }
                    }
                    foreach ($mois as $key=>$value){
                        if(date('F',strtotime('+'.$i.' days'))==$key){
                            $moisfr=$value;
                        }
                    }
                    ?>
                    <div id='js-<?php echo $i ?>' class="dropdown">
                        <button id="js-<?php echo $i?>" class="bloc-top">
                            <span><?php echo strtolower($jourfr).' '.date('d',strtotime('+'.$i.' days')) .' '. strtolower($moisfr) ?></span>
                            <span><i class="fa-solid fa-caret-down"></i></span>
                        </button>
                        <?php if(date('l',strtotime('+'.$i.' days'))=='Sunday'){
                            echo '';
                        }elseif(date('l',strtotime('+'.$i.' days'))=='Monday'){ ?>
                            <div class="horaire">
                                <?php
                                $compteur = 0;
                                for($y = idate('H',strtotime($planning->lundi_start)); $y<=idate('H',strtotime($planning->lundi_end));$y++){ ?>
                                    <div class="heure">
                                        <?php
                                        $nbre_heure = idate('H',strtotime($planning->lundi_end))-idate('H',strtotime($planning->lundi_start))+1;
                                        echo $form->inputRadio('heure',$y.':00-'.$i ,strval($nbre_heure-$compteur),'radio',date('y-m-d H:i',strtotime('+'.$i.' days '.date('H:i', strtotime($planning->lundi_start)).'+'.$compteur.' hours')));
                                        echo $form->label($y.':00-'.$i,date('H:i',strtotime($y.':00')));
                                        $compteur+=1;
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php }elseif (date('l',strtotime('+'.$i.' days'))=='Tuesday'){?>
                            <div class="horaire">
                                <?php
                                $compteur = 0;
                                for($y = idate('H',strtotime($planning->mardi_start)); $y<=idate('H',strtotime($planning->mardi_end));$y++){ ?>
                                    <div class="heure">
                                        <?php
                                        $nbre_heure = idate('H',strtotime($planning->mardi_end))-idate('H',strtotime($planning->mardi_start))+1;
                                        echo $form->inputRadio('heure',$y.':00-'.$i ,strval($nbre_heure-$compteur),'radio',date('y-m-d H:i',strtotime('+'.$i.' days '.date('H:i', strtotime($planning->mardi_start)).'+'.$compteur.' hours')));
                                        echo $form->label($y.':00-'.$i,date('H:i',strtotime($y.':00')));
                                        $compteur+=1;
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php }elseif (date('l',strtotime('+'.$i.' days'))=='Wednesday'){?>
                            <div class="horaire">
                                <?php
                                $compteur = 0;
                                for($y = idate('H',strtotime($planning->mercredi_start)); $y<=idate('H',strtotime($planning->mercredi_end));$y++){ ?>
                                    <div class="heure">
                                        <?php
                                        $nbre_heure = idate('H',strtotime($planning->mercredi_end))-idate('H',strtotime($planning->mercredi_start))+1;
                                        echo $form->inputRadio('heure',$y.':00-'.$i ,strval($nbre_heure-$compteur),'radio',date('y-m-d H:i',strtotime('+'.$i.' days '.date('H:i', strtotime($planning->mercredi_start)).'+'.$compteur.' hours')));
                                        echo $form->label($y.':00-'.$i,date('H:i',strtotime($y.':00')));
                                        $compteur+=1;
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php }elseif (date('l',strtotime('+'.$i.' days'))=='Thursday'){?>
                            <div class="horaire">
                                <?php
                                $compteur = 0;
                                for($y = idate('H',strtotime($planning->jeudi_start)); $y<=idate('H',strtotime($planning->jeudi_end));$y++){ ?>
                                    <div class="heure">
                                        <?php
                                        $nbre_heure = idate('H',strtotime($planning->jeudi_end))-idate('H',strtotime($planning->jeudi_start))+1;
                                        echo $form->inputRadio('heure',$y.':00-'.$i ,strval($nbre_heure-$compteur),'radio',date('y-m-d H:i',strtotime('+'.$i.' days '.date('H:i', strtotime($planning->jeudi_start)).'+'.$compteur.' hours')));
                                        echo $form->label($y.':00-'.$i,date('H:i',strtotime($y.':00')));
                                        $compteur+=1;
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php }elseif (date('l',strtotime('+'.$i.' days'))=='Friday'){?>
                            <div class="horaire">
                                <?php
                                $compteur = 0;
                                for($y = idate('H',strtotime($planning->vendredi_start)); $y<=idate('H',strtotime($planning->vendredi_end));$y++){ ?>
                                    <div class="heure">
                                        <?php
                                        $nbre_heure = idate('H',strtotime($planning->vendredi_end))-idate('H',strtotime($planning->vendredi_start))+1;
                                        echo $form->inputRadio('heure',$y.':00-'.$i ,strval($nbre_heure-$compteur),'radio',date('y-m-d H:i',strtotime('+'.$i.' days '.date('H:i', strtotime($planning->vendredi_start)).'+'.$compteur.' hours')));
                                        echo $form->label($y.':00-'.$i,date('H:i',strtotime($y.':00')));
                                        $compteur+=1;
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php }elseif (date('l',strtotime('+'.$i.' days'))=='Saturday'){?>
                            <div class="horaire">
                                <?php
                                $compteur = 0;
                                for($y = idate('H',strtotime($planning->samedi_start)); $y<=idate('H',strtotime($planning->samedi_end));$y++){ ?>
                                    <div class="heure">
                                        <?php
                                        $nbre_heure = idate('H',strtotime($planning->samedi_end))-idate('H',strtotime($planning->samedi_start))+1;
                                        echo $form->inputRadio('heure',$y.':00-'.$i ,strval($nbre_heure-$compteur),'radio',date('y-m-d H:i',strtotime('+'.$i.' days '.date('H:i', strtotime($planning->samedi_start)).'+'.$compteur.' hours')));
                                        echo $form->label($y.':00-'.$i,date('H:i',strtotime($y.':00')));
                                        $compteur+=1;
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>

                <?php } ?>
            </div>
            <?php echo $form->error('heure'); ?>
        </div>
        <?php echo $form->submit('submitted','Suivant'); ?>
    </form>
</div>