<div class="wrap">
    <div class="titre">
        <h2>Mes prochaines gardes d'enfants</h2>
    </div>
    <div id="gardes">
        <?php foreach ($infoReserves as $infoReserve){
            $jour=['Monday'=>'Lundi','Tuesday'=>'Mardi','Wednesday'=>'Mercredi','Thursday'=>'Jeudi','Friday'=>'Vendredi','Saturday'=>'Samedi','Sunday'=>'Dimanche'];
            $mois = ['January'=>'Janvier','February'=>'Février','March'=>'Mars','April'=>'Avril','May'=>'Mai','June'=>'Juin','July'=>'Juillet','August'=>'Août','September'=>'Septembre','October'=>'Octobre','November'=>'Novembre','December'=>"Decembre"];
            foreach ($jour as $key=>$value){
                if(date('l',strtotime($infoReserve->getDateStart()))==$key){
                    $jourfr=$value;
                }
            }
            foreach ($mois as $key=>$value){
                if(date('F',strtotime($infoReserve->getDateStart()))==$key){
                    $moisfr=$value;
                }
            }
            ?>
            <div class="reserv">
                <div class="date">
                    <div class="trg"></div>
                    <span><?php echo $jourfr.' '.date('d',strtotime($infoReserve->getDateStart())).' '.$moisfr.' à ' .date('H:i',strtotime($infoReserve->getDateStart())) ?></span>
                    <div class="trd"></div>
                </div>
                <div class="bloc">
                    <div class="infop">
                        <strong><?php echo ucfirst($infoReserve->prenom).' '.strtoupper($infoReserve->nom) ?></strong>
                        <p><i class="fa-solid fa-phone"></i> <?php echo $infoReserve->telephone ?></p>
                    </div>
                    <div class="separator"></div>
                    <div class="infoe">
                        <strong>Info de l'enfant : <?php echo ucfirst($infoReserve->prenom_enfant).' '.strtoupper($infoReserve->nom_enfant) ?></strong>
                        <div class="single">
                            <p><a href="<?php echo $view->path('SingleEnfant',array('id'=>$infoReserve->id_enfant))?>">Ses infos</a></p>
                        </div>
                    </div>
                    <div class="separator"></div>
                    <div class="infor">
                        <p>Temps de garde : <?php echo $infoReserve->getNbreHeure().'h' ?></p>
                        <p>Prix Total : <?php echo $infoReserve->prix_totale ?>€</p>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

</div>