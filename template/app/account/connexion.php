<div class="wrap2">
    <div class="titre">
        <h2>Connexion</h2>
    </div>
    <form id="formulaire" action="" class="add" method="post" novalidate>
        <div class="bloc">
            <?php echo $form->label('email', 'Email') ?>
            <?php echo $form->input('email', 'email') ?>
            <?php echo $form->error('login') ?>

            <?php echo $form->label('password', 'Mot de passe') ?>
            <?php echo $form->input('password', 'password') ?>
        </div>
        <?php echo $form->submit('submitted', 'Connexion') ?>
    </form>
</div>