<div class="wrap2">
    <div class="titre">
        <h2>Inscription</h2>
    </div>
    <form id="formulaire"  action="" class="add" method="post" novalidate>
        <div class="bloc">
            <div class="selectRole">
                <?php echo $form->label('role', 'Rôle') ?>
                <?php echo $form->select('role',$listRole, $account->role ?? '') ?>
                <?php echo $form->error('role') ?>
            </div>

            <?php echo $form->label('email', 'Email') ?>
            <?php echo $form->input('email', 'email') ?>
            <?php echo $form->error('email') ?>

            <?php echo $form->label('password', 'Mot de passe') ?>
            <?php echo $form->input('password', 'password') ?>
            <?php echo $form->error('password') ?>

            <?php echo $form->label('Confirmer mot de passe', 'Confirmer le mot de passe') ?>
            <?php echo $form->input('password2', 'password') ?>
        </div>

        <?php echo $form->submit('submitted', 'Inscription') ?>
    </form>
</div>