<section id="accueil">
    <div class="wrap">
        <div class="accueil_box">
            <div class="accueil_text">
                <h1><?= $title; ?></h1>
                <p><?= $presentation; ?></p>
                <a href="<?= $view->path('Inscription'); ?>"><?= $button; ?></a>
            </div>
            <div class="accueil_img">
                <img src="<?= $view->asset('img/girl_2.png'); ?>" alt="">
            </div>
        </div>
    </div>
</section>

<div class="transition_nuage">
    <img src="<?= $view->asset('img/nuage.svg'); ?>" alt="">
</div>

<section id="information">
    <div class="wrap">
       <div class="flexx">
            <button class="buttonn" id="bouton1">
                <img src="<?= $view->asset('img/mif.jpg'); ?>" alt="Icone Famille" />
                Parents
            </button>

            <button class="buttonn" id="bouton2">
                <img src="<?= $view->asset('img/nou.jpg'); ?>" alt="Icone Nounou" />
                Professionnel
            </button>
       </div>
        <div class="info">

        </div>
    </div>
</section>

