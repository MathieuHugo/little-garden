<div class="wrap mention">
    <h2>Mentions légales</h2>
    <p>Dernière mise à jour octobre 2023</p>

    <h2>Identification du site</h2><br>
    <p>Le propriétaire de ce site est l'association Groupe d'Études en Préventologie, immatriculée au Registre National
        des Associations sous le numéro W332008923 et au répertoire SIRENE sous l'identifiant 514 695 665, domiciliée au
        48, rue de Canolle à Bordeaux, ci-après le « GEP ». Le GEP est joignable par téléphone au +33 547 47 74 23 ou
        par mail à l'adresse contact@mesvaccins.net</p>

    <p>Le directeur de la publication est le Professeur Jean-Louis KOECK.</p>

    <p>Il a été réalisé par la société SYADEM, immatriculée au RCS de Bordeaux sous l'identifiant 789 255 320 et
        domiciliée au 48, rue de Canolle à Bordeaux. SYADEM est joignable par téléphone au +33 556 58 02 56 ou par mail
        à l'adresse contact@syadem.fr</p>

    <h2>L'hébergement du site est assuré par :</h2>

    <p>La société VERCEL pour la présentation de l'interface utilisateur. Aucune donnée n'est transmise à cet hébergeur.
        VERCEL Inc. est domiciliée au 340, South Lemon Avenue à Walnut, Californie, USA<br>
        La société GPLExpert pour la conservation et le traitement des données soumises. GPLExpert est immatriculée au
        RCS d'Evry sous le numéro 503 102 352 et domiciliée au 27, avenue Aristide Briand à Arpajon. Elle est joignable
        au téléphone au +33 182 52 20 62.<br>
        Description du service fourni<br>
        Le service CVN.MesVaccins.net permet à ses utilisateurs de :</p>

    <p>Créer un compte personnel d'accès<br>
        Créer un ou plusieurs carnets de vaccination numériques (CVN)<br>
        Partager des CVN avec d'autres utilisateurs ou des professionnels de santé<br>
        Accéder à des carnets de vaccination numériques créés par des professionnels de santé ou par d'autres
        utilisateurs et partagés avec ce compte personnel<br>
        Consulter ou modifier les CVN auxquels ils ont accès<br>
        Obtenir un audit vaccinal basé sur les données présentes dans un CVN<br>
        Révoquer des partages de CVN précédemment accordés<br>
        Supprimer les CVN qu'ils ont créés<br>
        Transférer la propriété d'un CVN à un autre compte personnel d'accès<br>
        Plus de détails sur l'usage du service sont consultables dans le centre d'aide du CVN.</p>

    <h3>Gestion des données personnelles<br>
        Responsabilités</h3><br>
    </p>Le GEP est responsable du traitement des données à caractère personnel effectué par ce service. Il en sous-traite la réalisation à SYADEM, qui lui-même recourt aux services de l'hébergeur GPLExpert.</p>

    <p>GPLExpert est certifié en tant qu'hébergeur de données de santé.</p>

    <p>Données traitées<br>
        Les données traitées concernent les utilisateurs, les titulaires de CVN et les professionnels de santé.</p>

    <p>Les données concernant les utilisateurs sont celles permettant leur authentification lors de la connexion au site
        : adresse mail, mot de passe et numéro de téléphone portable utilisé pour l'authentification renforcée.</p>

    <p>Les données concernant les titulaires de CVN sont :</p>

    <p>Les traits d'identité permettant de sécuriser le partage avec les professionnels de santé : nom de naissance, nom
        d'usage, prénom, date et lieu de naissance, code postal du lieu de résidence, identifiant national de santé,
        sexe.<br>
        Les données strictement nécessaires à l'établissement de l'audit vaccinal, qui comportent :<br>
        Des données de vie personnelle, tant sur la personne elle-même (conditions de logement, grossesse, ...) que sur
        son entourage (jeunes enfants, personnes fragiles, ...)<br>
        Des données de vie professionnelle, pour les professions faisant l'objet de recommandations vaccinales
        spécifiques.<br>
        Des données de santé : antécédents de maladies infectieuses, allergies, maladies chroniques, traitement en
        cours, résultats d'examens et de dosages, ...<br>
        D'autres données sensibles : vie sexuelle, toxicomanie, vie en institut psychiatrique ou en prison, ...<br>
        L'historique vaccinal : vaccin utilisé, date d'administration, numéro de lot, détails sur l'injection,
        d'éventuels commentaires sur des effets indésirables, l'identité d'un professionnel de santé ayant validé
        l'enregistrement de l'acte.<br>
        Les données concernant les professionnels de santé sont celles nécessaires à leur identification par les
        utilisateurs lors des partages de CVN : profession, nom et prénom, lieu d'exercice.</p>

    <h2>Base juridique du traitement</h2><br>
    <h3>Le traitement décrit ci-dessus est fondé sur :</h3>

    <p>Le consentement de l'utilisateur pour les données concernant les utilisateurs.<br>
        Le consentement du titulaire, recueilli soit par l'utilisateur, soit par un professionnel ou un autre
        utilisateur ayant partagé le CVN pour les données concernant un titulaire de CVN.<br>
        L'intérêt légitime du GEP et des utilisateurs pour les données concernant les professionnels de santé. La
        réutilisation de ces données publiquement disponibles permet aux utilisateurs de partager plus sûrement les CVN
        avec les professionnels qui prennent en charge les titulaires.<br>
        Origine des données</p><br>
    <h3>Les données sont issues :</h3>

    <p>Des saisies effectuées par les utilisateurs pour eux-mêmes ou pour le compte des titulaires de carnets,<br>
        Des saisies effectuées par les professionnels de santé dans leurs propres outils lorsque ceux-ci accèdent à des
        carnets partagés.<br>
        De l'annuaire santé national pour les données concernant les professionnels de santé.<br>
        Destinataires des données / Transferts de données<br>
        Les destinataires des données sont les autres utilisateurs et les professionnels avec lesquels des partages sont
        actifs.</p>

    <p>Dans le cas de partage avec des professionnels, ce partage constitue un transfert vers un autre traitement, opéré
        sous la responsabilité du destinataire. Lors de la révocation d'un partage avec un professionnel, il conservera
        une copie du CVN à son <p>État avant la révocation.</p>

    <h4>Aucun transfert n'est effectué vers des pays tiers.</h4>

    <p>Durée de conservation<br>
        La durée de conservation d'un CVN est de 20 ans après la dernière connexion de l'utilisateur qui l'a créé ou en
        a reçu la propriété.</p>

    <p>Droits des personnes concernées<br>
        Les personnes concernées, qu'il s'agisse des utilisateurs, des titulaires de carnets ou des professionnels de
        santé bénéficient de l'ensemble des droits qui leur sont accordés par le Règlement Général sur la Protection des
        Données.</p>

    <h2>Droit d'accès</h2>
    <p>Les utilisateurs ont accès sur le site à l'ensemble des données qui les concernent.</p>

    <p>Les titulaires de carnets, s'ils en sont le propriétaire ou si le propriétaire leur a accordé un droit d'accès,
        ont accès sur le site à l'ensemble des données qui les concernent. Dans le cas contraire, si le carnet leur a
        été associé de manière sûre à l'occasion du partage avec un professionnel de santé, ils peuvent s'adresser à ce
        professionnel pour obtenir une copie en propre du carnet. Enfin, à défaut de pouvoir recourir à un professionnel
        connu, ils peuvent depuis le Centre d'Aide (https://support-cvn.mesvaccins.net) solliciter le support
        utilisateur qui les mettra en relation avec un professionnel pouvant effectuer l'opération.</p>

    <p>Les données concernant les professionnels sont les données publiques de l'Annuaire santé.</p>

    <h3>Droit de rectification</h3>
    <p>Les utilisateurs peuvent modifier sur le site l'ensemble des données qui les concernent.</p>

    <p>Les titulaires, via les utilisateurs ayant accès à leur CVN, peuvent modifier les données uniquement tant
        qu'elles n'ont pas été validées par un professionnel de santé bénéficiant d'un partage. Les professionnels de
        santé bénéficiant d'un partage peuvent modifier les données du profil vaccinal et celles de l'historique
        vaccinal lorsqu'elles ont été initialement validées sur présentation d'une preuve documentaire. Les données de
        l'historique vaccinal validées lors de l'acte d'administration du vaccin ne peuvent être modifiées que par le
        professionnel de santé ayant effectué et validé l'acte. Les titulaires depuis le Centre d'Aide solliciter le
        support utilisateur pour faire invalider des données précédemment validées, ce qui les rend modifiables à
        nouveau.</p>

    <p>Les professionnels de santé peuvent depuis le Centre d'Aide solliciter le support utilisateur pour faire modifier
        leurs données, après vérification de leur identité.</p>

    <p>Droit à l'effacement<br>
        Les utilisateurs peuvent via le Centre d'Aide demander la résiliation de leurs comptes dès lors que ceux-ci ne
        sont plus détenteurs d'aucun carnet. Cette résiliation sera effective sous un délai d'au plus une semaine après
        la demande.</p>

    <p>Les utilisateurs peuvent effacer ou céder la propriété des CVN dont ils sont propriétaires. En cas d'effacement,
        ces carnets disparaîtront également des autres comptes utilisateurs qui les auraient reçus en partage.</p>

    <p>Les professionnels de santé peuvent via le Centre d'Aide demander l'effacement de leurs données. Ils ne seront
        alors plus en mesure de recevoir des partages de CVN. Cet effacement sera effectif sous un délai d'au plus une
        semaine après la demande.</p>

    <p>Droit à la limitation du traitement<br>
        Hormis en cas de partage, les données ne sont traitées que sur action explicite d'un utilisateur. Les
        utilisateurs ayant la possibilité de révoquer l'ensemble des partages, ils peuvent ainsi réaliser la limitation
        du traitement à la seule conservation des données.</p>

    <p>Droit d'opposition<br>
        Le droit d'opposition, qui ne concerne que les traitements mis en œuvre au motif d'intérêt public ou d'intérêt
        légitime du responsable de traitement, s'applique aux professionnels de santé. Ils peuvent via le Centre d'Aide
        demander l'effacement de leurs données. Cet effacement sera effectif sous un délai d'au plus une semaine après
        la demande.</p>

    <p>Droit à la portabilité des données<br>
        Tout utilisateur, propriétaire d'un CVN ou y ayant accès, peut en exporter les données dans un format conforme
        au cadre d'interopérabilité national des systèmes d'information de santé.</p>
    <p>Droit de retrait du consentement<br>
        La suppression d'un CVN ou d'un compte vaut retrait du consentement au traitement.</p>

    <p>Droit de réclamation<br>
        Les personnes concernées n'ayant pu obtenir satisfaction auprès du Délégué à la protection des données
        (dpo@mesvaccins.net) sont en droit de saisir la Commission Nationale Informatique et Libertés selon sa procédure
        décrite en https://www.cnil.fr/saisir-la-cnil.</p>

    <p>Traitements ultérieurs<br>
        Le GEP peut procéder à un traitement d'anonymisation des données collectées à des fins d'études scientifiques ou
        statistiques, au seul usage des autorités sanitaires (Haute Autorité de Santé, Agence nationale de santé
        publique, INSERM, Autorités régionales de santé, Ministère de la Santé).</p>

    <p>Cookies et données persistantes<br>
        Le service CVN ne dépose de cookies sur le poste de l'utilisateur que si celui-ci sollicite l'enregistrement de
        celui-ci en tant que poste de confiance après une authentification renforcée, permettant ainsi une
        authentification simple pour ses accès ultérieurs.</p>

    <p>Propriété intellectuelle et contrefaçon<br>
        La structure générale du site, ainsi que les textes, graphiques, images, sons et vidéos sont la propriété de
        SYADEM. Toute représentation, reproduction, exploitation partielle ou totale de ce site, par quelque procédé que
        ce soit, sans l'autorisation expresse et préalable de SYADEM est interdite et constituerait une contrefaçon au
        sens des articles L 335-2 et suivants du code de la propriété intellectuelle.</p>

    <p>Qualité des données<br>
        Les règles permettant de d'établir l'audit vaccinal à partir des caractéristiques individuelles saisies sont
        élaborées par les experts en vaccinologie de SYADEM à partir de documents publics.</p>

    <p>Il s'agit notamment des résumés des caractéristiques de produits, des avis du Haut Conseil de la santé publique
        (HCSP) et de la Haute Autorité de Santé (HAS), des informations de l'Agence Nationale de Sécurité du Médicament
        (ANSM) et de l'Agence Européenne des Médicaments (EMA), ou encore des données épidémiologiques de Santé Publique
        France ou du Centre européen pour la prévention et le contrôle des maladies (ECDC).</p>

    <p>Les documents sources sont systématiquement présentés en complément d'information des recommandations
        correspondantes.</p>

    <p>Les règles sont révisées dans les meilleurs délais en cas de modification des textes de référence, soit en moins
        de deux jours ouvrés pour 90% des modifications, et dans tous les cas en moins de deux mois.</p>

    <p>Le processus de publication des règles comporte une revue par un second expert sur la base d'un test automatisé
        systématique des réponses modifiées.</p>
</div>