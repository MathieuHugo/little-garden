<h1>Conditions Générales d'Utilisation (CGU)</h1>

<h3>Dernière mise à jour : 11/03/2024</h3>

<p>Bienvenue sur Little Garden ! Merci de lire attentivement ces Conditions Générales d'Utilisation (CGU) avant d'utiliser nos services.</p>

<h3>Acceptation des conditions</h3>
<p>En accédant et en utilisant Little Garden, vous acceptez pleinement et sans réserve les présentes CGU. Si vous n'acceptez pas ces conditions, veuillez ne pas utiliser notre plateforme.</p>

<h3>Description du service</h3>
<p>Des professionnels de la garde d'enfant propose leurs services et les parents recherchant une
    solution de garde ponctuelle ou régulière pour leurs enfants.</p>

<h3>Compte utilisateur</h3>
<p>a. Inscription et confidentialité</p>

<p>Pour accéder à certaines fonctionnalités de notre plateforme, vous devrez créer un compte. Vous êtes responsable de la confidentialité de vos informations d'identification et de toutes les activités qui se produisent sous votre compte. Informez-nous immédiatement de toute utilisation non autorisée de votre compte.</p>

<p>b. Exactitude des informations</p>

<p>Vous vous engagez à fournir des informations exactes, complètes et à jour lors de l'inscription. Nous nous réservons le droit de suspendre ou de résilier votre compte si les informations fournies sont inexactes.</p>

<h3>Utilisation du service</h3>
<p>a. Comportement de l'utilisateur</p>

<p>Vous vous engagez à utiliser notre plateforme conformément aux lois en vigueur et aux présentes CGU. Il est interdit de publier, de partager ou de distribuer tout contenu illégal, diffamatoire, offensant, pornographique, ou toute autre activité contraire à l'éthique.</p>

<p>b. Propriété intellectuelle</p>

<p>Tout le contenu présent sur Little Garden, y compris le texte, les images, les vidéos, les logos, et autres éléments, est protégé par des droits de propriété intellectuelle. Vous vous engagez à respecter ces droits et à ne pas reproduire, distribuer, ou créer des œuvres dérivées sans autorisation.</p>

<h3>Modification et résiliation</h3>
<p>Nous nous réservons le droit de modifier, suspendre ou résilier votre accès à Little Garden à tout moment, pour quelque raison que ce soit, avec ou sans préavis.</p>

<h3>Limitation de responsabilité</h3>
<p>[Clause de non-responsabilité indiquant que la plateforme est fournie "telle quelle" et que l'utilisation est à vos propres risques.]</p>

<h3>Loi applicable et juridiction</h3>
<p>Les présentes CGU sont régies par les lois en vigueur dans Rouen. Tout litige découlant de l'utilisation de notre plateforme sera soumis à la compétence exclusive des tribunaux de Rouen.</p>

<p>Merci de prendre le temps de lire attentivement ces Conditions Générales d'Utilisation. Si vous avez des questions ou des préoccupations, n'hésitez pas à nous contacter à [adresse e-mail de support].</p>

<p>Fait à Rouen, le 11/03/2024.</p>