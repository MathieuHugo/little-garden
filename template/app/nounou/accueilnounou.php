<div class="index">
    <div class="love">
        <h3>Trouver votre bonheur parmi les familles : le coin des nounous choyées et valorisées !</h3>
    </div>
    <div class="infos">
        <img src="<?= $view->asset('img/adult.png'); ?>" alt="logo">
    </div>
</div>
<div class="res">
    <h1>Mes prochaines gardes</h1>
</div>
<div class="appointments-container">
    <?php foreach($infoReserves as $infoReserve){
        $jour=['Monday'=>'Lundi','Tuesday'=>'Mardi','Wednesday'=>'Mercredi','Thursday'=>'Jeudi','Friday'=>'Vendredi','Saturday'=>'Samedi','Sunday'=>'Dimanche'];
        $mois = ['January'=>'Janvier','February'=>'Février','March'=>'Mars','April'=>'Avril','May'=>'Mai','June'=>'Juin','July'=>'Juillet','August'=>'Août','September'=>'Septembre','October'=>'Octobre','November'=>'Novembre','December'=>"Decembre"];
        foreach ($jour as $key=>$value){
            if(date('l',strtotime($infoReserve->getDateStart()))==$key){
                $jourfr=$value;
            }
        }
        foreach ($mois as $key=>$value){
            if(date('F',strtotime($infoReserve->getDateStart()))==$key){
                $moisfr=$value;
            }
        }
        ?>
    <div class="appointment-card">
        <div class="appointment-header">
            <span class="appointment-day"><?php echo $jourfr.' '.date('d',strtotime($infoReserve->getDateStart())).' '.$moisfr ?></span>
            <span class="appointment-time"><?php echo date('H:i',strtotime($infoReserve->getDateStart())) ?></span>
        </div>
        <div class="appointment-info">
            <img class="profile-pic" src="<?= $view->asset('img/fame.jpg'); ?>" alt="logo">
            <div>
                <div class="appointment-name"><?php echo $infoReserve->prenom .' '. $infoReserve->nom ?></div>
                <div class="appointment-title"><?php echo $infoReserve->telephone ?></div>
            </div>
        </div>
        <div class="appointment-footer">
           Enfant : <?php echo $infoReserve->prenom_enfant .' '. $infoReserve->nom_enfant ?>
        </div>
    </div>
    <?php } ?>
</div>

<div class="card">
    <div class="card-image-container">
        <img src="<?= $view->asset('img/amour.png'); ?>" alt="" class="card-image">
    </div>
    <div class="card-content">
        <h2>Un blog réservé aux parents</h2>
        <p>Nos meilleurs conseils pour les nouveaux parents</p>
        <a href="" class="card-button">Rejoindre</a>
    </div>
</div>

<div class="card2">
    <div class="card-image-container2">
        <img src="<?= $view->asset('img/love.png'); ?>" alt="" class="card-image2">
    </div>
    <div class="card-content2">
        <h2>Vous voulez nous contacter ?</h2>
        <p>Notre équipe est disponible 24h/24</p>
        <a href="" class="card-button2">Nous contacter</a>
    </div>
</div>
