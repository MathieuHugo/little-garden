<div class="wrap2">
    <form id="formulaire" action="" method="post" novalidate>
        <h1>Ajouter votre planning</h1>
        <div class="bloc">
            <?php
            $jours=['lundi','mardi','mercredi','jeudi','vendredi','samedi'];
            foreach ($jours as $jour) {
                echo $form->label($jour.'_start',ucfirst($jour).' - Heure de début');
                echo $form->input($jour.'_start', 'time');
                echo $form->error($jour.'_start');

                echo $form->label($jour.'_end',ucfirst($jour).' - Heure de fin');
                echo $form->input($jour.'_end', 'time');
                echo $form->error($jour.'_end');
            }
            ?>
        </div>
        <?php echo $form->submit('submitted','Ajouter') ?>
    </form>
</div>