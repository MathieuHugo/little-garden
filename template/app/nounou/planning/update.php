<div class="wrap2">
    <form id="formulaire" action="" method="post" novalidate>
        <h1>Modifier votre planning</h1>
        <div class="bloc">
            <?php
            echo $form->label('lundi_start','Lundi - Heure de début');
            echo $form->input('lundi_start', 'time',$planning->getLundiStart());
            echo $form->error('lundi_start');

            echo $form->label('lundi_end','Lundi - Heure de fin');
            echo $form->input('lundi_end', 'time',$planning->getLundiEnd());
            echo $form->error('lundi_end');

            echo $form->label('mardi_start','Mardi - Heure de début');
            echo $form->input('mardi_start', 'time',$planning->getMardiStart());
            echo $form->error('mardi_start');

            echo $form->label('mardi_end','Mardi - Heure de fin');
            echo $form->input('mardi_end', 'time',$planning->getMardiEnd());
            echo $form->error('mardi_end');

            echo $form->label('mercredi_start','Mercredi - Heure de début');
            echo $form->input('mercredi_start', 'time',$planning->getMercrediStart());
            echo $form->error('mercredi_start');

            echo $form->label('mercredi_end','Mercredi - Heure de fin');
            echo $form->input('mercredi_end', 'time',$planning->getMercrediEnd());
            echo $form->error('mercredi_end');

            echo $form->label('jeudi_start','Jeudi - Heure de début');
            echo $form->input('jeudi_start', 'time',$planning->getJeudiStart());
            echo $form->error('jeudi_start');

            echo $form->label('jeudi_end','Jeudi - Heure de fin');
            echo $form->input('jeudi_end', 'time',$planning->getJeudiEnd());
            echo $form->error('jeudi_end');

            echo $form->label('vendredi_start','Vendredi - Heure de début');
            echo $form->input('vendredi_start', 'time',$planning->getVendrediStart());
            echo $form->error('vendredi_start');

            echo $form->label('vendredi_end','Vendredi - Heure de fin');
            echo $form->input('vendredi_end', 'time',$planning->getvendrediEnd());
            echo $form->error('vendredi_end');

            echo $form->label('samedi_start','Samedi - Heure de début');
            echo $form->input('samedi_start', 'time',$planning->getSamediStart());
            echo $form->error('samedi_start');

            echo $form->label('samedi_end','Samedi - Heure de fin');
            echo $form->input('samedi_end', 'time',$planning->getSamediEnd());
            echo $form->error('samedi_end');
            ?>
        </div>
        <?php echo $form->submit('submitted','Ajouter') ?>
    </form>
</div>