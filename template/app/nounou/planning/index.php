<div class="wrap">
    <div id="planningNounou">
        <?php if(!empty($planning)){ ?>
        <div class="titre">
            <h1>Horaire par jour</h1>
        </div>
        <div class="planning">
            <ul>
                <li>Le lundi : <?php echo $planning->getLundiStart().' - '.$planning->getLundiEnd() ?></li>
                <li>Le mardi : <?php echo $planning->getMardiStart().' - '.$planning->getMardiEnd() ?></li>
                <li>Le mercredi : <?php echo $planning->getMercrediStart().' - '.$planning->getMercrediEnd() ?></li>
                <li>Le jeudi : <?php echo $planning->getJeudiStart().' - '.$planning->getJeudiEnd() ?></li>
                <li>Le vendredi : <?php echo $planning->getVendrediStart().' - '.$planning->getVendrediEnd() ?></li>
                <li>Le samedi : <?php echo $planning->getSamediStart().' - '.$planning->getSamediEnd() ?></li>
                <li>Le dimanche : Jour de repos</li>
            </ul>
            <div class="image">
                <img src="<?php echo $view->asset('img/planning.png') ?>" alt="image de représentation d'un planning">
            </div>
        </div>
        <div class="modifPlanning">
            <p><a href="<?php echo $view->path('ModifPlanning') ?>">Modifier mon planning</a></p>
        </div>
        <?php }else{ ?>
            <div class="titre">
                <h1>Remplissez votre planning</h1>
            </div>
            <div class="addPlanning">
                <p><a href="<?php echo $view->path('AjoutPlanning') ?>">Remplir mon planning</a></p>
            </div>
        <?php } ?>
    </div>
</div>