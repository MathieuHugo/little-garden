import {dropdownInfo} from "./settingDebug";

const dropdowns = dropdownInfo();

export function dropdownCreate(){
    const debugBar = document.querySelector('#debug-bar')

    let divOpen = document.createElement('div');
    let divClose = document.createElement('div');
    let h2 = document.createElement('h2');
    let divAllDropdown = document.createElement('div');
    let iOpen = document.createElement('i');
    let iClose = document.createElement('i');

    divOpen.id = 'debug-bar-open-button'
    divClose.id = 'debug-bar-close-button'
    divOpen.classList.add('debug-bar-open-button')
    divClose.classList.add('debug-bar-close-button')
    iOpen.classList.add('fa-solid')
    iOpen.classList.add('fa-gear')
    iClose.classList.add('fa-solid')
    iClose.classList.add('fa-xmark')
    h2.classList.add('debug-bar-title')
    h2.innerText = 'DEBUG BAR';
    divAllDropdown.classList.add('debug-bar-all-dropdown-box')

    divOpen.appendChild(iOpen)
    divClose.appendChild(iClose)
    debugBar.append(divOpen)
    debugBar.append(divClose)
    debugBar.append(h2)

    for (let i=0; i<dropdowns.length; i++){
        let dropdown = document.createElement('div');
        let dropdownHead = document.createElement('div');
        let dropdownContent = document.createElement('div');
        dropdown.classList.add('debug-bar-dropdown-one')
        dropdownHead.id = dropdowns[i]['open'].substring(1)
        dropdownHead.classList.add('debug-bar-dropdown-head')
        dropdownHead.innerHTML = dropdowns[i]['name']+' <span>^</span>'
        dropdownContent.id = dropdowns[i]['content'].substring(1)
        dropdownContent.classList.add('debug-bar-dropdown-content')
        dropdown.appendChild(dropdownHead)
        dropdown.appendChild(dropdownContent)
        divAllDropdown.appendChild(dropdown)
    }
    debugBar.append(divAllDropdown)
}
