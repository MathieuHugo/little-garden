export function host()
{
    return 'http://' + window.location.host
}

export function role()
{
    const roles=['admin','user','banni','new']
    return roles
}

export function dropdownInfo()
{
    const dropdowns = [
        { open: '#debug-bar-dropdown-info-head', content: '#debug-bar-dropdown-info-content', name: 'Info Page' },
        { open: '#debug-bar-dropdown-performance-head', content: '#debug-bar-dropdown-performance-content', name: 'Performance' },
        { open: '#debug-bar-dropdown-error-head', content: '#debug-bar-dropdown-error-content', name: 'Error' },
        { open: '#debug-bar-dropdown-log-head', content: '#debug-bar-dropdown-log-content', name: 'Log' },
        { open: '#debug-bar-dropdown-route-head', content: '#debug-bar-dropdown-route-content', name: 'Routes' },
        { open: '#debug-bar-dropdown-request-head', content: '#debug-bar-dropdown-request-content', name: 'Requêtes' },
        { open: '#debug-bar-dropdown-global-head', content: '#debug-bar-dropdown-global-content', name: '$Global' },
        { open: '#debug-bar-dropdown-personalisation-head', content: '#debug-bar-dropdown-personalisation-content', name: 'Personalisation' }
    ];
    return dropdowns;
}

export function dropdownStyle() {
    const themes =
        [
            { number: 1, debugBarColor: '#7E9ABF', debugBarTextColor: '#202020', debugBarOpenButtonColor: '#202020', debugBarCloseButtonColor: 'red', debugBarContentColor: 'ghostwhite', debugBarBorderColor: 'white' },
            { number: 2, debugBarColor: 'firebrick', debugBarTextColor: 'white', debugBarOpenButtonColor: 'white', debugBarCloseButtonColor: 'antiquewhite', debugBarContentColor: 'darkred', debugBarBorderColor: 'darkred' },
            { number: 3, debugBarColor: '#F2A88D', debugBarTextColor: '#A63333', debugBarOpenButtonColor: 'antiquewhite', debugBarCloseButtonColor: 'antiquewhite', debugBarContentColor: 'ghostwhite', debugBarBorderColor: 'white' },
            { number: 4, debugBarColor: '#3C7DA6', debugBarTextColor: '#EC5C42', debugBarOpenButtonColor: 'white', debugBarCloseButtonColor: 'antiquewhite', debugBarContentColor: 'ghostwhite', debugBarBorderColor: 'white' },
            { number: 5, debugBarColor: '#C8E6C9', debugBarTextColor: '#006400', debugBarOpenButtonColor: '#006400', debugBarCloseButtonColor: '#006400', debugBarContentColor: 'ghostwhite', debugBarBorderColor: '#006400' },
        ]

    return themes;
}