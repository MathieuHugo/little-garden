import {host} from "./settingDebug";

export async function getError(errorjs) {
    let params = new FormData();
    const res = await fetch(host()+'/get-error', {
        body: params,
        method: 'post',
    });
    const data = await res.json();

    const errorBox = document.querySelector('#debug-bar-dropdown-error-content')
    const ul = document.createElement('ul')

    ul.id="listeError"
    ul.classList.add('liste-element')

    if (data.length == 0 && errorjs.length == 0){
        ul.innerText = 'Aucune Erreur sur le site'
        ul.style.textAlign = 'center';
    }

    if (data.length > 0){
        ul.innerHTML = 'Erreur Php: '
        for (let i = 0; i < data.length; i++){
            let li = document.createElement('li');
            li.classList.add('element');
            li.innerText = data[i]['errstr']+' à la ligne '+data[i]['errline']+' dans '+data[i]['errfile'].split("\\")[data[i]['errfile'].split("\\").length - 1];
            ul.appendChild(li)
        }
    }

    if (errorjs.length > 0){
        ul.innerHTML = ul.innerHTML+'Erreur Js: '
        for (let i = 0; i < errorjs.length; i++){
            let li = document.createElement('li');
            li.classList.add('element');
            li.innerText = errorjs[i]['message']+' dans '+errorjs[i]['source'].split("/")[errorjs[i]['source'].split("/").length - 1].split("?")[0];
            ul.appendChild(li)
        }
    }

    if (errorBox){
        errorBox.append(ul)
    }
}