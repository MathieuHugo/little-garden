import {host} from './settingDebug'
export async function getRoutes() {
    let params = new FormData();
    const res = await fetch(host()+'/get-routes', {
        body: params,
        method: 'post',
    });
    const data = await res.json();
    const boxRoutes=document.querySelector('#debug-bar-dropdown-route-content')
    const listeRoute=document.createElement('table')
    listeRoute.classList.add('table-route')
    const headertable=document.createElement('thead')
    const listeentete=document.createElement('tr')
    const urlListe=document.createElement('th')
    urlListe.innerText='URL:'
    const controllerListe=document.createElement('th')
    controllerListe.innerText='Controller:'
    const methodListe=document.createElement('th')
    methodListe.innerText='Methode:'
    const bodytable=document.createElement('tbody')
    listeentete.appendChild(urlListe)
    listeentete.appendChild(controllerListe)
    listeentete.appendChild(methodListe)
    headertable.appendChild(listeentete)
    listeRoute.appendChild(listeentete)
    listeRoute.appendChild(bodytable)
    data.forEach((d)=>{
        const route =document.createElement('tr')
        for (let i = 0; i < 3; i++) {

            if(d[1]=== 'DebugBar'){
            }else{
                const elementRoute = document.createElement('th')
                elementRoute.innerText=d[i]
                route.appendChild(elementRoute)
                elementRoute.addEventListener('click',(evt)=>{
                    document.location.href=host()+'/'+d[0]
                })
            }


        }
        bodytable.appendChild(route)
    })
    if (boxRoutes){
        boxRoutes.append(listeRoute)
    }
}

