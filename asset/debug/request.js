import {host} from "./settingDebug";

export async function getRequest() {
    let params = new FormData();
    const res = await fetch(host()+'/get-request', {
        body: params,
        method: 'post',
    });
    const data = await res.json();

    const requestBox = document.querySelector('#debug-bar-dropdown-request-content')


    let ul = document.createElement('ul')
    ul.id="listeGet"
    ul.classList.add('liste-element')


    if (data.length > 0){
        ul.innerHTML = ul.innerHTML+'Requêtes : '

        for (let i =0; i<data.length; i++){
            let li = document.createElement('li');
            li.classList.add('element');
            li.innerText = data[i];
            ul.appendChild(li)
        }
    }else{
        ul.innerText = 'Aucune Requête sur le site'
        ul.style.textAlign = 'center';
    }

    if (requestBox){
        requestBox.append(ul)
    }
}