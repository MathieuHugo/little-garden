
var select = document.querySelector("select");
var heures = document.querySelectorAll("input[type='radio']");
var label = document .querySelectorAll("label")

//console.table(label);
select.addEventListener('change', function(e) {
    e.preventDefault();
    heures.forEach(function (heure){
        if(parseInt(select.value) > parseInt(heure.classList['value'])){
            heure.disabled=true;
            label.forEach(function (lab){
                //console.log(heure.htmlFor);
                if(heure.id===lab.htmlFor){
                    lab.style.textDecoration = 'line-through';
                }
            })
        }else if (parseInt(select.value)<=parseInt(heure.classList['value'])){
            heure.disabled=false;
            label.forEach(function (lab){
                //console.log(heure.htmlFor);
                if(heure.id===lab.htmlFor){
                    lab.style.textDecoration = 'none';
                }
            })
        }
    })
})

const dropdowns = document.querySelectorAll(".dropdown");
const btnDrops = document.querySelectorAll(".bloc-top");


btnDrops.forEach(function (btnDrop){
    dropdowns.forEach(function (dropdowm){
        let toggleIndex = 0;
        btnDrop.addEventListener('click',(evt)=>{
            evt.preventDefault();

            // console.log(dropdown.scrollHeight);
            if (toggleIndex === 0 && btnDrop.id === dropdowm.id){
                dropdowm.style.height = `${dropdowm.scrollHeight}px`;
                toggleIndex++;
            }else{
                dropdowm.style.height = `${btnDrop.scrollHeight}px`;
                toggleIndex--;
            }

        })
    })
})