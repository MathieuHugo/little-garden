import './app/scss/home.scss';

console.log('home.js');

window.addEventListener('DOMContentLoaded', (event) => {
    var bouton1 = document.getElementById("bouton1");
    var bouton2 = document.getElementById("bouton2");
    var infosDiv = document.querySelector(".info");

    function afficherInfos1() {
        var imageUrl = "asset/img/girl_1.png";
        console.log(imageUrl);
        infosDiv.innerHTML = `
            <div class='info_img'>
                <img src='${imageUrl}' alt=''>
            </div>
            <div class='info_text'>
                <h2>Trouvez la garderie parfaite pour votre enfant !</h2>
                <div class='text_one'>
                    <div class='icone'><i class="fa-solid fa-magnifying-glass"></i></div>
                    <div class='texte'>
                        <h3>Recherche personnalisée</h3>
                        <p>Découvrez notre outil de recherche avancé qui vous permet de filtrer les garderies par emplacement, type de pédagogie, disponibilité, et plus encore.</p>
                    </div>
                </div>
                <div class='text_two'>
                    <div class='icone'><i class="fa-regular fa-star"></i></div>
                    <div class='texte'>
                        <h3>Sécurité et Confiance</h3>
<p>Chaque nounou est soigneusement contrôlée pour assurer la sécurité et la tranquillité des familles.</p>
                    </div>
                </div>
                <div class='text_three'>
                    <div class='icone'><i class="fa-solid fa-user-check"></i></div>
                    <div class='texte'>
                        <h3>Inscription et suivi en ligne</h3>
                        <p>Simplifiez votre vie grâce à notre système d'inscription en ligne. Réservez une place, gérez votre profil et suivez le développement de votre enfant.</p>
                    </div>
                </div>
            </div>`;
    }

    function afficherInfos2() {
        var imageUrl = "asset/img/girl_2.png";
        infosDiv.innerHTML = `
            <div class='info_img'>
                <img src='${imageUrl}' alt=''>
            </div>
            <div class='info_text'>
                <h2>Connectez-vous avec des Familles Locales</h2>
                <div class='text_one'>
                    <div class='icone'><i class="fa-regular fa-eye"></i></div>
                    <div class='texte'>
                        <h3>Visibilité améliorée</h3>
                       <p>Mettez en avant votre service de garde et votre approche pédagogique auprès de familles proches.</p>
                    </div>
                </div>
                <div class='text_two'>
                    <div class='icone'><i class="fa-solid fa-list-check"></i></div>
                    <div class='texte'>
                        <h3>Gestion simplifiée</h3>
                        <p>Utilisez notre plateforme pour gérer facilement les inscriptions, les disponibilités, et communiquer avec les parents.</p>
                    </div>
                </div>
                <div class='text_three'>
                    <div class='icone'><i class="fa-regular fa-comments"></i></div>
                    <div class='texte'>
                        <h3>Retours et évolution</h3>
                        <p>Bénéficiez des retours constructifs des parents pour améliorer continuellement vos services.</p>
                    </div>
                </div>
            </div>`;
    }

    afficherInfos1();

    bouton1.addEventListener("click", afficherInfos1);
    bouton2.addEventListener("click", afficherInfos2);
});
