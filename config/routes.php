<?php

$routes = array(
    // DefaultController
    array('home','default','index'), // URL - Controller - méthode
    array('Mention-Legales','default','mention'),
    array('CGU','default','cgu'),
    array('403','default','page403'),

    // AccountController
    array('Inscription', 'account', 'inscription'),
    array('Connexion', 'account', 'connection'),
    array('Deconnexion', 'account', 'deconnection'),

    ////////////////////////////////////////////////////
    // debugBarRoute
    array('get-routes','DebugBar','getroutes'),
    array('get-error','DebugBar','geterror'),
    array('get-global','DebugBar','getglobal'),
    array('get-request','DebugBar','getrequest'),
    array('get-personalisation','DebugBar','ergonomie'),
    array('set-personalisation','DebugBar','setergonomie'),
    array('login','DebugBar','login'),
    array('logout','DebugBar','logout'),

    /////////////////////////////////////////////////
    //PlanningController
    array('MonPlanning','planning','index'),
    array('AjoutPlanning','planning','add'),
    array('ModifPlanning','planning','update'),
    array('MesGardes','planning','gardes'),

    //ProfilNounouController
    array('Profil-nounou','profilnounou','index'),

    // ProfilProController
    array('editProfil','profilPro','edit'),
    array('verify','profilPro','verify'),

    //////////////////////////////////////////////////////////
    //AccueilParentsController
    array('AccueilParents','accueilparents','accueil'),

    //FamilleController
    array('familles', 'famille', 'index'),
    array('singleFamille', 'famille', 'single', array('id')),
    array('add-famille', 'famille', 'add'),
    array('edit-famille', 'famille', 'edit', array('id')),
    array('delete-famille', 'famille', 'delete', array('id')),

    //ListeNounouController
    array('ListeNounou','listenounou','index',array('search')),
    array('SingleNounou','listenounou','single',array('id')),
    array('ReservationNounou','listenounou','reservation',array('id')),
    array('ConfirmationReservation','listenounou','confirmation',array('id')),
    array('Facture','listeNounou','facture',array('id')),

    //MesReservationsController
    array('MesReservations','mesreservations','index'),
    array('ModifierMaReservation','mesreservations','modif',array('id')),
    array('SupprimerMaReservation','mesreservations','delete',array('id')),

    //ProfilEnfantsController
    array('MesEnfants','profilenfants','index'),
    array('InscrireMonEnfant','profilenfants','add'),
    array('SingleEnfant','profilenfants','single',array('id')),
    array('ModifierMonEnfant','profilenfants','modif',array('id')),
    array('SupprimerMonEnfant','profilenfants','delete',array('id')),

    //ProfilParentsController
    array('ModifierMonProfil','profilparents','modif',array('id')),

    //StripeController
    array('checkout','Stripe','checkout'),
    array('success','Stripe','success'),
    array('payer', 'Stripe', 'payment'),

    // Partie Admin
    array('admin', 'admin', 'dashboard'),
    array('admin-parent', 'admin', 'parent'),
    array('admin-pro', 'admin', 'professionnel'),
    array('statut-pro', 'admin', 'status',(array('id'))),
);
