<?php

namespace App\Service;

class Upload{

    public static function Upload($type){
        $file_name = $_FILES[$type]['name'];// le nom du fichier
        if(!is_dir("upload")) {
            mkdir('upload');
        }
        if(!is_dir("upload/".date('Y'))) {
            mkdir("upload/".date('Y'));
        }
        // extension
        $i_point = strrpos($file_name,'.');
        $fileExtension = substr($file_name, $i_point ,strlen($file_name) - $i_point);

        $newNameOfTheFichier = date('Y_m_d_H_i_s') . rand(10, 29077) .'_original' . $fileExtension;

        $file_tmp  = $_FILES[$type]['tmp_name'];  // le chemin du fichier temporaire
        if(move_uploaded_file($file_tmp,"upload/".date('Y'). '/' .$newNameOfTheFichier )) {
            return $newNameOfTheFichier;
        }
    }
}