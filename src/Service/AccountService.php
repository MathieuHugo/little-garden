<?php

namespace App\Service;

class AccountService
{
    public function isLogged() {
        $key = 'usersoid555564546Tuydy6';
        if(!empty($_SESSION[$key]['id'])) {
            if(!empty($_SESSION[$key]['email'])) {
                if(!empty($_SESSION[$key]['ip'])) {
                    if($_SESSION[$key]['ip'] === $_SERVER['REMOTE_ADDR']) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function generateToken($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}