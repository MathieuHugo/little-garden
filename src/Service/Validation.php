<?php
namespace App\Service;

class Validation
{
    protected $errors = array();

    public function IsValid($errors)
    {
        foreach ($errors as $key => $value) {
            if(!empty($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * emailValid
     * @param email $email
     * @return string $error
     */

    public function emailValid($email)
    {
        $error = '';
        if(empty($email) || (filter_var($email, FILTER_VALIDATE_EMAIL)) === false) {
            $error = 'Adresse email invalide.';
        }
        return $error;
    }

    /**
     * textValid
     * @param POST $text string
     * @param title $title string
     * @param min $min int
     * @param max $max int
     * @param empty $empty bool
     * @return string $error
     */

    public function textValid($text, $title, $min = 3,  $max = 50, $empty = true)
    {

        $error = '';
        if(!empty($text)) {
            $strtext = strlen($text);
            if($strtext > $max) {
                $error = 'Votre ' . $title . ' est trop long.';
            } elseif($strtext < $min) {
                $error = 'Votre ' . $title . ' est trop court.';
            }
        } else {
            if($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }
        return $error;

    }

    public function validPassword($password, $password2)
    {
        $error = '';
        if(!empty($password) && !empty($password2)) {
            if($password != $password2) {
                $error = 'Vos mots de passe sont différents';
            } elseif(mb_strlen($password) < 6) {
                $error = 'Votre mot de passe est trop court(min 6)';
            }
        } else {
            $error = 'Veuillez renseigner les mots de passe';
        }
        return $error;
    }

    public function fileValid ($file, $size_max = 2000000,$goodExtension = array('image/jpeg', 'image/png', 'image/jpg'))
    {
        $error = '';
        if ($file['error'] > 0) {
            if ($file['error'] != 4) {
                $error = 'Problem: ' . $file['error'] . '<br />';
            }else {
                $error = 'Aucun fichier n\'a été téléchargé';
            }
        }else{
            $file_size = $file['size'];
            $file_tmp  = $file['tmp_name'];

            if($file_size > $size_max || filesize($file_tmp) > $size_max) {
                $error = 'Le fichier est trop gros (max '.$size_max.' octets)';
            } else {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime = finfo_file($finfo, $file_tmp);
                finfo_close($finfo);

                if (!in_array($mime, $goodExtension)) {
                    $str = '';
                    foreach($goodExtension as $ext) {
                        $str .= $ext . ',';
                    }
                    $error = 'Veuillez télécharger une image de type ' .substr($str, 0, -1);
                }
            }
        }
        return $error;
    }


    private function age($date) {
        $age = date('Y') - date('Y', strtotime($date));
        if (date('md') < date('md', strtotime($date))) {
            return $age - 1;
        }
        return $age;
    }


    public function dateNaissanceValid($text){
        $error='';
        if(!empty($text)){
            if(strtotime($text)>=time()){
                $error='Veuillez renseigner une date passée';
            }else{
                $age = date('Y') - date('Y', strtotime($text));
                if (date('md') < date('md', strtotime($text))) {
                    $age -= 1;
                }
                if($age>=18){
                    $error='La personne n\'est pas un enfant';
                }elseif ($age<=1){
                    $error='L\'enfant est trop petit pour accéder au service';
                }
            }
        }else{
            $error='Veuillez renseigner une date de naissance';
        }
        return $error;
    }

    public function validateCityName($cityName, $title = 'nom de la ville', $min = 2, $max = 85, $empty = true) {
        return $this->textValid($cityName, $title, $min, $max, $empty);
    }

    public function validatePostalAddress($postalAddress, $title = 'adresse postale', $min = 5, $max = 120, $empty = true) {
        return $this->textValid($postalAddress, $title, $min, $max, $empty);
    }

    public function validatePhoneNumber($phoneNumber, $title = 'numéro de téléphone', $min = 10, $max = 20, $empty = true) {
        $error = $this->textValid($phoneNumber, $title, $min, $max, $empty);
        if(empty($error) && !preg_match('/^[0-9]+$/', $phoneNumber)) {
            $error = 'Votre ' . $title . ' doit contenir uniquement des chiffres.';
        }
        return $error;
    }

}
