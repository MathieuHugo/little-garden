<?php

namespace App\Controller;

use App\Model\ProfilparentsModel;
use App\Service\AccountService;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\Config;

class AccountController extends BaseController
{
    private $v;
    private $account;
    private $listRole;

    public function __construct()
    {
        $this->v = new Validation();
        $this->account = new AccountService();

        $this->listRole = (new Config())->get('listRole');
    }

    public function inscription(){
        if ($this->isLogged()) {$this->Abort403();}

        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);

            $errors = $this->validateRegister($post);

            if ($this->v->IsValid($errors)) {
                $passwordP = password_hash($post['password'], PASSWORD_DEFAULT);
                $token = $this->account->generateToken(80);

                ProfilparentsModel::insertUser($post, $passwordP, $token);

                $user = ProfilparentsModel::findByColumn('email',$post['email']);

                $_SESSION['usersoid555564546Tuydy6'] = array(
                    'id' => $user->getId(),
                    'email' => $user->getEmail(),
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'role' => $user->getRole(),
                );

                if ($_SESSION['usersoid555564546Tuydy6']['role'] === "nanny"){
                    $this->redirect('editProfil');
                } elseif ($_SESSION['usersoid555564546Tuydy6']['role'] === "parent") {
                    $this->redirect('AccueilParents');
                }
            }
        }
        $form = new Form($errors);

        $this->render('app.account.register',array(
            'listRole' => $this->listRole,
            'form' => $form,
        ));
    }

    public function connection(){
        if ($this->isLogged()) {$this->Abort403();}

        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);

            $user = ProfilparentsModel::findByColumn('email', $post['email']);

            if (!empty($user)) {
                if(!password_verify($post['password'], $user->getPassword())) {
                    $errors = 'Mot de passe incorrect !';
                } else {
                    if (!empty($user->getNom())){
                        $_SESSION['usersoid555564546Tuydy6'] = array(
                            'id' => $user->getId(),
                            'email' => $user->getEmail(),
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'role' => $user->getRole(),
                            'nom' => $user->getNom(),
                            'prenom' => $user->getPrenom(),
                            'adresse' => $user->getAdresse(),
                            'code_postale' => $user->getCodePostale(),
                            'ville' => $user->getVille(),
                            'tel' => $user->getTelephone(),
                            'token' => $user->getToken(),
                        );
                    } else {
                        $_SESSION['usersoid555564546Tuydy6'] = array(
                            'id' => $user->getId(),
                            'email' => $user->getEmail(),
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'role' => $user->getRole(),
                        );
                    }
                    if ($_SESSION['usersoid555564546Tuydy6']['role'] === "nanny"){
                        $this->redirect('editProfil');
                    } elseif ($_SESSION['usersoid555564546Tuydy6']['role'] === "parent"){
                        $this->redirect('AccueilParents');
                    } elseif ($_SESSION['usersoid555564546Tuydy6']['role'] === "admin"){
                        $this->redirect('admin');
                    } else {
                        $this->Abort404();
                    }
                }
            } else {
                $errors['login'] = 'Compte inexistant';
            }
        }
        $form = new Form($errors);

        $this->render('app.account.connexion',array(
            'form' => $form,
        ));
    }

    public function deconnection(){
        if (!$this->isLogged()) {$this->Abort403();}

        $_SESSION = array();
        session_destroy();
        $this->redirect('');
    }

    private function validateRegister($post) {
        $errors['email'] = $this->v->emailValid($post['email']);

        if(empty($errors['email'])) {
            $userVerif = ProfilparentsModel::findByColumn('email', $post['email']);
            if(!empty($userVerif)) {
                $errors['email'] = 'Cet email existe déjà';
            }
        }
        $errors['password'] = $this->v->validPassword($post['password'],$post['password2']);
        return $errors;
    }
}