<?php
namespace App\Controller;

use App\Model\ProfilenfantsModel;
use App\Model\ProfilparentsModel;
use App\Model\ProModel;
use App\Model\ReserveModel;
use App\Service\Form;
use App\Service\Validation;

class MesreservationsController extends BaseController{

    public function __construct(){
        if ($_SESSION['usersoid555564546Tuydy6']['role'] === 'nanny'){
            $this->Abort403();
        }
        if (empty($_SESSION['usersoid555564546Tuydy6']['nom'])){
            $this->redirect('AccueilParents');
        }
    }

    public function index(){
        $infoReserves = ReserveModel::infoReservationByIdUser($_SESSION['usersoid555564546Tuydy6']['id']);
        $this->render('app.parent.mesreservations.index',array(
            'infoReserves'=>$infoReserves,
        ),'parents');
    }

    public function modif($id){
        $enfants = ProfilenfantsModel::findByIdFalse($_SESSION['usersoid555564546Tuydy6']['id'],'id_user');
        $id_pro = ReserveModel::findIdProByJoin($id);
        $planning = ProModel::selectPlanningByJoinById($id_pro->getId());
        $errors=[];
        if(!empty($_POST['submitted'])){
            $post=$this->cleanXss($_POST);
            $v=new Validation();
            if(empty($post['heure'])) {
                $errors['heure'] = 'Veuillez choisir une date et un horaire !';
            }
            if($v->IsValid($errors)){
                ReserveModel::update($post,$id);
                $this->redirect('ConfirmationReservation',array('id'=>$id));
            }
        }
        $form=new Form($errors);
        $this->render('app.parent.mesreservations.modif',array(
            'planning'=>$planning,
            'enfants'=>$enfants,
            'form'=>$form,
        ),'parents');
    }

    public function delete($id){
        $reservation=ReserveModel::findById($id);
        if(empty($reservation)){
            $this->Abort404();
        }
        ReserveModel::delete($id);
        $this->redirect('ListeNounou/all');
    }
}