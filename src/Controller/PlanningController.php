<?php
namespace App\Controller;

use App\Model\PlanningModel;
use App\Model\ProModel;
use App\Model\ReserveModel;
use App\Service\Form;
use App\Service\Validation;

class PlanningController extends BaseController{

    public function __construct(){
        $proUserProfil = ProModel::findByColumn('id_user',$_SESSION['usersoid555564546Tuydy6']['id']);
        if (!$this->isLogged() || $_SESSION['usersoid555564546Tuydy6']['role'] != "nanny") {
            $this->Abort403();
        }
        if ($proUserProfil->getStatut() != 'actif'){
            $this->redirect('verify');
        }
    }

    public function index(){
        $id_pro = ProModel::findIdByIdUser($_SESSION['usersoid555564546Tuydy6']['id']);
        $planning = PlanningModel::findAllByIdPro($id_pro->getId());
        $this->render('app.nounou.planning.index',array(
            'planning'=>$planning,
        ),'nounou');
    }

    public function add(){
        $id_pro = ProModel::findIdByIdUser($_SESSION['usersoid555564546Tuydy6']['id']);
        $planning = PlanningModel::findAllByIdPro($id_pro->getId());
        if(!empty($planning)){
            $this->Abort404();
        }
        if(!empty($_POST['submitted'])){
            $posts = $this->cleanXss($_POST);
            $jours=['lundi','mardi','mercredi','jeudi','vendredi','samedi'];
            foreach ($posts as $key=>$post){
                if(!empty($posts[$key])) {
                    if (date('i',strtotime($posts[$key]))!=0){
                        $errors[$key] = 'Veuillez renseigner 0 minutes !';
                    }
                    foreach ($jours as $jour){
                        if(date('H',strtotime($posts[$jour.'_end']))<date('H',strtotime($posts[$jour.'_start']))){
                            $errors[$key] = 'Veuillez renseigner une heure de fin inférieur à l\'heure de début !';
                        }
                    }
                }else{
                    $errors[$key] = 'Veuillez renseigner un horaire !';
                }
            }
            $v=new Validation();
            if($v->IsValid($errors)){
                PlanningModel::insert($id_pro->getId(),$posts);
                $this->redirect('MonPlanning');
            }
        }
        $form=new Form($errors);
        $this->render('app.nounou.planning.add',array(
            'form'=>$form,
        ),'nounou');
    }

    public function update(){
        $id_pro = ProModel::findIdByIdUser($_SESSION['usersoid555564546Tuydy6']['id']);
        $planning = PlanningModel::findAllByIdPro($id_pro->getId());
        if(empty($planning)){
            $this->Abort404();
        }
        if(!empty($_POST['submitted'])){
            $posts = $this->cleanXss($_POST);
            $jours=['lundi','mardi','mercredi','jeudi','vendredi','samedi'];
            foreach ($posts as $key=>$post){
                if(!empty($posts[$key])) {
                    if (date('i',strtotime($posts[$key]))!=0){
                        $errors[$key] = 'Veuillez renseigner 0 minutes !';
                    }
                    foreach ($jours as $jour){
                        if(date('H',strtotime($posts[$jour.'_end']))<date('H',strtotime($posts[$jour.'_start']))){
                            $errors[$key] = 'Veuillez renseigner une heure de fin inférieur à l\'heure de début !';
                        }
                    }
                }else{
                    $errors[$key] = 'Veuillez renseigner un horaire !';
                }
            }
            $v=new Validation($errors);
            if($v->IsValid($errors)){
                PlanningModel::update($id_pro->getId(),$posts);
                $this->redirect('MonPlanning');
            }
        }
        $form=new Form($errors);
        $this->render('app.nounou.planning.update',array(
            'planning'=>$planning,
            'form'=>$form,
        ),'nounou');
    }

    public function gardes(){
        $id_pro = ProModel::findIdByIdUser($_SESSION['usersoid555564546Tuydy6']['id']);
        $infoReserves = ReserveModel::infoReservationByIdPro($id_pro->getId());
        $this->render('app.nounou.planning.gardes',array(
            'infoReserves'=>$infoReserves,
        ),'nounou');
    }

}