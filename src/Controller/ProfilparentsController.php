<?php
namespace App\Controller;

use App\Model\ProfilparentsModel;
use App\Service\Form;
use App\Service\Validation;

class ProfilparentsController extends BaseController {

    public function __construct(){
        if ($_SESSION['usersoid555564546Tuydy6']['role'] === 'nanny'){
            $this->Abort403();
        }
    }

    public function modif($id){
        if ($_SESSION['usersoid555564546Tuydy6']['id'] != $id){
            $this->redirect('403');
        }
        $users = ProfilparentsModel::findById($id);
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors['adresse'] = $v->textValid($post['adresse'],'adresse',2,70);
            $errors['nom'] = $v->textValid($post['nom'],'nom',2,80);
            $errors['prenom'] = $v->textValid($post['prenom'],'prenom',2,80);
            $errors['ville'] = $v->textValid($post['ville'],'ville',2,80);
            $errors['code_postal'] = $v->validatePostalAddress($post['code_postal'], 'code_postal');
            $errors['tel'] = $v->validatePhoneNumber($post['tel'], 'numéro de téléphone');
            if($v->IsValid($errors)){
                ProfilparentsModel::ubdateUser($post,$_SESSION);
                $user = ProfilparentsModel::findById($_SESSION['usersoid555564546Tuydy6']['id']);
                $_SESSION['usersoid555564546Tuydy6'] = array(
                    'id' => $user->getId(),
                    'email' => $user->getEmail(),
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'role' => $user->getRole(),
                    'nom' => $user->getNom(),
                    'prenom' => $user->getPrenom(),
                    'adresse' => $user->getAdresse(),
                    'code_postale' => $user->getCodePostale(),
                    'ville' => $user->getVille(),
                    'tel' => $user->getTelephone(),
                    'token' => $user->getToken(),
                );
                $this->redirect('AccueilParents');
            }
        }
        $form = new Form($errors);
        $this->render('app.parent.profilparents.modif',array(
            'form'=> $form,
            'users' => $users
        ),'parents');
    }

}