<?php


namespace App\Controller;


use App\Service\Form;
use Core\Kernel\AbstractController;


class DebugBarController extends BaseController
{
    public function getRoutes()
    {
        $config = include  dirname(__DIR__) . '../../config/config-dist.php';
        if ($config['mode'] == 'prod'){
            $this->redirect('');
        }

        include '../config/routes.php';
        $this->showJson($routes);
    }

    public function geterror()
    {
        $config = include  dirname(__DIR__) . '../../config/config-dist.php';
        if ($config['mode'] == 'prod'){
            $this->redirect('');
        }

        if (isset($_SESSION['errors'])){
            $allErrors = $_SESSION['errors'];
        }else{
            $allErrors = [];
        }
        unset($_SESSION['errors']);
        $this->showJson($allErrors);
    }


    public function login()
    {
        $config = include  dirname(__DIR__) . '../../config/config-dist.php';
        if ($config['mode'] == 'prod'){
            $this->redirect('');
        }

        $_SESSION['user']=['role'=>$_POST['role']];
        $this->showJson($_SESSION['user']);
    }
    public function logout()
    {
        $config = include  dirname(__DIR__) . '../../config/config-dist.php';
        if ($config['mode'] == 'prod'){
            $this->redirect('');
        }

        $_SESSION['user']=[];
        session_destroy();
    }

    public function getglobal()
    {
        $config = include  dirname(__DIR__) . '../../config/config-dist.php';
        if ($config['mode'] == 'prod'){
            $this->redirect('');
        }

        $global['post'] = [];
        $global['file'] = [];
        $global['session'] = [];
        $global['cookie'] = [];

        foreach ($_FILES as $key => $value) {
            $global['file'][] = ['key'=>$key,'value'=>$value];
        }

        foreach ($_SESSION as $key => $value) {
            if ($key != 'errors' && $key != 'debug_bar' && $key != 'debugpost' && $key != 'debug_bar_sql'){
                $global['session'][] = ['key'=>$key,'value'=>$value];
            }
        }

        foreach ($_COOKIE as $key => $value) {
            if ($key != 'verif_set_error'){
                $global['cookie'][] = ['key'=>$key,'value'=>$value];
            }
        }

        foreach ($_SESSION['debugpost'] as $key => $value) {
            $global['post'][] = ['key'=>$key,'value'=>$value];
        }
        $this->showJson($global);
    }

    public function getrequest()
    {
        $config = include  dirname(__DIR__) . '../../config/config-dist.php';
        if ($config['mode'] == 'prod'){
            $this->redirect('');
        }

        if (isset($_SESSION['debug_bar_sql'])){
            $sql = $_SESSION['debug_bar_sql'];
        }else{
            $sql = [];
        }
        unset($_SESSION['debug_bar_sql']);

        $this->showJson($sql);
    }

    public function ergonomie()
    {
        $config = include  dirname(__DIR__) . '../../config/config-dist.php';
        if ($config['mode'] == 'prod'){
            $this->redirect('');
        }

        if (!isset($_SESSION['debug_bar'])){
            $_SESSION['debug_bar'] = ['position'=>'right','theme'=>1];
        }
        $this->showJson($_SESSION['debug_bar']);
    }

    public function setergonomie()
    {
        $config = include  dirname(__DIR__) . '../../config/config-dist.php';
        if ($config['mode'] == 'prod'){
            $this->redirect('');
        }

        $_SESSION['debug_bar'][$_POST['type']] = $_POST['value'];
    }
}