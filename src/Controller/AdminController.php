<?php
namespace App\Controller;

use App\Model\ListenounouModel;
use App\Model\ProfilenfantsModel;
use App\Model\ProfilparentsModel;
use App\Model\ProModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\Config;

class AdminController extends BaseController{

    public function __construct(){
        if ($_SESSION['usersoid555564546Tuydy6']['role'] != "admin"){
            $this->Abort403();
        }
    }

    public function dashboard()
    {
        $title = 'Dashboard';

        $countParent = ProfilparentsModel::count();

        $countPro = ProModel::count();

        $countEnfant = ProfilenfantsModel::count();



        $this->render('app.admin.dashboard',array(
            'title' => $title,
            'countParent'=>$countParent,
            'countPro'=>$countPro,
            'countEnfant'=>$countEnfant,
        ),'admin');
    }

    public function parent(){
        $title = 'Gestion des parents';

        $parents = ProfilparentsModel::findByColumnAll('role', 'parent');

        $this->render('app.admin.parent',array(
            'title' => $title,
            'parents' => $parents,
        ),'admin');
    }

    public function professionnel()
    {
        $title = 'Gestion des professionnels';

        $pros = ListenounouModel::getListAndPro();

        $this->render('app.admin.professionnel', array(
            'title' => $title,
            'pros' => $pros,
        ), 'admin');
    }

    public function status($id)
    {
        $title = 'Valider le/la professionnel';
        $errors = array();
        $statut = $this->getStatutByIdOr404($id);
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            ProModel::updateProStatut($post, $id);
            $this->redirect('admin');
        }
        $form = new Form($errors);
        $this->render('app.admin.statut',array(
            'title' => $title,
            'statut' => $statut,
            'form'=>$form,
        ),'admin');
    }

    private function validate($v, $post) {
        $errors = array();

        return $errors;
    }

    private function getStatutByIdOr404($id) {
        $statut = ProModel::findById($id);
        if (empty($statut)) {
            $this->abort404();
        }
        return $statut;
    }

}