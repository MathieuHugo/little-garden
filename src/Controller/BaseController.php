<?php

namespace App\Controller;

use App\Service\AccountService;
use Core\Kernel\AbstractController;

class BaseController extends AbstractController
{

    public function isLogged() {
        $security = new AccountService();
        return $security->isLogged();
    }

}