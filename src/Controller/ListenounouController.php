<?php
namespace App\Controller;

use App\Model\CompetenceModel;
use App\Model\FacturationModel;
use App\Model\ListenounouModel;
use App\Model\PlanningModel;
use App\Model\ProCompetenceModel;
use App\Model\ProfilenfantsModel;
use App\Model\ProfilparentsModel;
use App\Model\ProModel;
use App\Model\ReserveModel;
use App\Service\Form;
use App\Service\Validation;
//use function PHPUnit\Framework\returnArgument;

class ListenounouController extends BaseController{

    public function __construct(){
        if ($_SESSION['usersoid555564546Tuydy6']['role'] === 'nanny'){
            $this->Abort403();
        }
        if (empty($_SESSION['usersoid555564546Tuydy6']['nom'])){
            $this->redirect('AccueilParents');
        }
    }

    public function index($search){
        if ($search != 'all'){
            $url = 'https://api-adresse.data.gouv.fr/search/?q='.$search.'&type=municipality&autocomplete=0';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            curl_close($curl);
            $data = json_decode($response, true);

            if ($data['features'][0]['properties']['municipality'] != $search){
                $this->redirect('ListeNounou/all');
            } else {
                $donnee = ListenounouModel::getUserByProVille($search);
                if (!$donnee[0]){
                    $this->redirect('ListeNounou/all');
                }
            }
        }else{
            $donnee = ListenounouModel::getUserByPro();
        }

        $erreur = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            if ($post['ville'] == ''){
                $post['ville'] = 'all';
            }

            $this->redirect('ListeNounou/'.$post['ville']);
        }
        $form = new Form($erreur);

        $this->render('app.parent.listenounou.index',array(
            'donnee'=>$donnee,
            'form'=>$form,
        ),'parents');
    }

    public function single($id){
        $pro = ProModel::findById($id);
        $user = ProfilparentsModel::findById($pro->getIdUser());
        $idCompetences = ProCompetenceModel::findCompetenceByIdArray($pro->id);
        $competence = array();
        for ($i = 0;$i <= count($idCompetences) - 1;$i++){
            $competence[$i] = CompetenceModel::findById($idCompetences[$i]->getIdCompetence());
        }


        $this->render('app.parent.listenounou.single',array(
            'pro'=>$pro,
            'user'=>$user,
            'competence' => $competence,
        ),'parents');
    }

    public function reservation($id){
        $enfants = ProfilenfantsModel::findByIdFalse($_SESSION['usersoid555564546Tuydy6']['id'],'id_user');
        if(empty($enfants)){
            $this->redirect('MesEnfants');
        }
        $planning = ProModel::selectPlanningByJoinById($id);
        $errors=[];
        if(!empty($_POST['submitted'])){
            $post=$this->cleanXss($_POST);
            $v=new Validation();
            if(empty($post['heure'])) {
                $errors['heure'] = 'Veuillez choisir une date et un horaire !';
            }
            if($v->IsValid($errors)){
                ReserveModel::insert($post,$_SESSION['usersoid555564546Tuydy6']['id'],$id);
                $lastId=ReserveModel::findLastId();
                $this->redirect('ConfirmationReservation',array('id'=>$lastId[0]->getId()));
            }
        }
        $form=new Form($errors);
        $this->render('app.parent.listenounou.reservation',array(
            'form'=>$form,
            'enfants'=>$enfants,
            'planning'=>$planning,
        ),'parents');
    }

    public function confirmation ($id){
        $infoReserv = ReserveModel::infoReservation($id);
        if(empty($infoReserv)){
            $this->Abort403();
        }
        $prix_totale = $infoReserv->getNbreHeure()*$infoReserv->tarif;
        if(!empty($_POST['submitted'])){
            FacturationModel::insert($_SESSION['usersoid555564546Tuydy6']['id'],$infoReserv->getIdPro(),$id,$prix_totale);
            $this->redirect('payer');
        }
        $form=new Form();
        $this->render('app.parent.listenounou.confirmation',array(
            'form'=>$form,
            'infoReserv'=>$infoReserv,
        ),'parents');
    }

    public function facture($id){
        $this->render('app.parent.listenounou.facture',array(),'parents');
    }

}