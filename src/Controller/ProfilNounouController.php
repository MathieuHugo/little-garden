<?php
namespace App\Controller;

use App\Model\ProModel;
use App\Model\ReserveModel;

class ProfilNounouController extends BaseController{

    public function __construct(){
        $proUserProfil = ProModel::findByColumn('id_user',$_SESSION['usersoid555564546Tuydy6']['id']);
        if (!$this->isLogged() || $_SESSION['usersoid555564546Tuydy6']['role'] != "nanny") {
            $this->Abort403();
        }
        if ($proUserProfil->getStatut() != 'actif'){
            $this->redirect('verify');
        }
    }

    public function index(){
        $id_pro = ProModel::findIdByIdUser($_SESSION['usersoid555564546Tuydy6']['id']);
        $infoReserves = ReserveModel::infoReservationByIdPro($id_pro->getId());
        $this->render('app.nounou.accueilnounou',array(
            'infoReserves'=>$infoReserves,
        ),'nounou');
    }


}