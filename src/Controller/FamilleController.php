<?php

namespace App\Controller;

use App\Model\FamilleModel;
use App\Service\Form;
use App\Service\Validation;

class FamilleController extends BaseController{

    public function __construct(){
        if ($_SESSION['usersoid555564546Tuydy6']['role'] === 'nanny'){
            $this->Abort403();
        }
    }

    public function index(){
        $familles = FamilleModel::all();
        $this->render('app.parent.famille.index', array(
            'familles' => $familles
        ), 'parents');
    }

    public function single($id){
        $famille = $this->getFamilyByIdOr404($id);
        $this->render('app.parent.famille.single', array(
            'famille' => $famille
        ), 'parents');
    }

    public function add(){
        $errors = array();
        if(!empty($_POST['submitted'])){
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v, $post);
            if($v->IsValid($errors)){
                FamilleModel::insert($post);
                $this->addFlash('success', 'Le membre de la famille a bien été ajouté.');
                $this->redirect('familles');
            }
        }
        $form = new Form($errors);
        $this->render('app.parent.famille.add', array(
            'form' => $form
        ), 'parents');
    }

    public function edit($id){
        $errors = [];
        $famille = $this->getFamilyByIdOr404($id);
        if(!empty($_POST['submitted'])){
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v, $post);
            if($v->IsValid($errors)){
                FamilleModel::update($post, $id);
                $this->addFlash('success', 'Ce membre a été modifié');
                $this->redirect('familles');
            }
        }
        $form = new Form($errors);
        $this->render('app.parent.famille.modif', array(
            'form' => $form
        ),'parents');
    }

    public function delete($id){
        $this->getFamilyByIdOr404($id);
        FamilleModel::delete($id);
        $this->addFlash('success', 'Le membre a bien été supprimé.');
        $this->redirect('familles');
    }

    private function getFamilyByIdOr404($id){
        $famille = FamilleModel::findById($id);
        if(empty($famille)){
            $this->Abort404();
        }
        return $famille;
    }

    private function validate($v, $post){
        $errors['nom'] = $v->textValid($post['nom'], 'nom', 2, 70);
        $errors['prenom'] = $v->textValid($post['prenom'], 'prenom', 2, 70);
        $errors['telephone'] = $v->textValid($post['telephone'], 'telephone', 2, 10);
        return $errors;
    }
}