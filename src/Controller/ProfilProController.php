<?php

namespace App\Controller;

use App\Model\CompetenceModel;
use App\Model\ProCompetenceModel;
use App\Model\ProfilparentsModel;
use App\Model\ProModel;
use App\Service\Form;
use App\Service\Upload;
use App\Service\Validation;
use function App\Model\insert;

class ProfilProController extends BaseController {

    public function __construct(){
        if (!$this->isLogged() || $_SESSION['usersoid555564546Tuydy6']['role'] != "nanny") {
            $this->Abort403();
        }
    }

    public function verify() {
        $this->render('app.pro.profil.verify',array(

        ),'nounou');
    }

    public function edit() {
        if (!empty($_SESSION['usersoid555564546Tuydy6']['nom'])){
            $this->redirect('Profil-nounou');
        }

        $competence = CompetenceModel::all();

        $error = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);

            $valid = new Validation();
            $error = $this->validate($valid,$post);

            if($valid->IsValid($error)) {
                $nameImg = Upload::Upload('image');
                $namePdf = Upload::Upload('pdf');

                ProModel::insert($post,$_SESSION,$nameImg,$namePdf);
                ProfilparentsModel::ubdateUser($post,$_SESSION);
                $proProfil = ProModel::findByColumn('id_user',$_SESSION['usersoid555564546Tuydy6']['id']);
                ProCompetenceModel::insert($post,$proProfil->getId());

                $user = ProfilparentsModel::findByColumn('nom', $post['nom']);
                $_SESSION['usersoid555564546Tuydy6'] = array(
                    'id' => $user->getId(),
                    'nom' => $user->getNom(),
                    'prenom' => $user->getPrenom(),
                    'adresse' => $user->getAdresse(),
                    'code_postale' => $user->getCodePostale(),
                    'ville' => $user->getVille(),
                    'telephone' => $user->getTelephone(),
                    'email' => $user->getEmail(),
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'role' => $user->getRole(),
                );
                $this->redirect('verify');
            }
        }

        $profil = new Form($error);

        $this->render('app.pro.profil.editProfil',array(
            'profil' => $profil,
            'competence' => $competence,
        ),'nounou');
    }

    private function validate($valid,$post) {
        $error['nom'] = $valid->textValid($post['nom'], 'nom', 2, 20);                          // nom
        $error['prenom'] = $valid->textValid($post['prenom'], 'prenom', 2, 20);                 // prenom
        $error['adresse'] = $valid->textValid($post['adresse'], 'adresse', 2, 255);             // adresse
        $error['code_postal'] = $valid->textValid($post['code_postal'], 'code_postal', 5, 5);   // code_postal
        if (!is_numeric($post['code_postal'])) {
            $error['code_postal'] = 'Ce doit etre un nombre!';
        }
        $error['ville'] = $valid->textValid($post['ville'], 'ville', 2, 20);                    // ville
        $error['tel'] = $valid->textValid($post['tel'], 'tel', 10, 10);                         // tel
        if (!is_numeric($post['tel'])) {
            $error['tel'] = 'Ce doit etre un nombre!';
        }
        $error['siren'] = $valid->textValid($post['siren'], 'siren', 9, 9);                     // siren
        if (!is_numeric($post['siren'])) {
            $error['siren'] = 'Ce doit etre un nombre!';
        }
        $error['siret'] = $valid->textValid($post['siret'], 'siret', 14, 14);                   // siret
        if (!is_numeric($post['siret'])) {
            $error['siret'] = 'Ce doit etre un nombre!';
        }
        $error['tarif'] = $valid->textValid($post['tarif'], 'tarif', 1, 3);                     // tarif
        if (!is_numeric($post['tarif'])) {
            $error['tarif'] = 'Ce doit etre un nombre!';
        }
        $error['max_enfant'] = $valid->textValid($post['max_enfant'], 'max_enfant', 1, 2);      // max_enfant
        if (!is_numeric($post['max_enfant'])) {
            $error['max_enfant'] = 'Ce doit etre un nombre!';
        }
        $error['image'] = $valid->fileValid($_FILES['image'], 2000000, array('image/jpeg', 'image/png', 'image/jpg'));
        $error['pdf'] = $valid->fileValid($_FILES['pdf'], 2000000, array('application/pdf'));

        $str= urlencode($post['adresse']);
        $str2= urlencode($post['ville']);
        $str3= urlencode($post['code_postal']);

        $url = 'https://api-adresse.data.gouv.fr/search/?q='.$str.'%20'.$str2.'%20'.$str3.'&type=housenumber&autocomplete=0';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response, true);

        if ($data['features'][0]['properties']['name'] != $post['adresse']){
            $error['adresse'] = 'erreur dans l\'adresse';
        }
        if ($data['features'][0]['properties']['city'] != $post['ville']){
            $error['ville'] = 'erreur dans la ville';
        }
        if ($data['features'][0]['properties']['postcode'] != $post['code_postal']){
            $error['code_postal'] = 'erreur dans la code postal';
        }
        return $error;
    }
}
