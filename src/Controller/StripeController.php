<?php
namespace App\Controller;

use App\Model\ProfilparentsModel;
use Stripe\Stripe;
use Stripe\Checkout\Session as CheckoutSession;
use App\Controller\BaseController;



class StripeController extends BaseController
{
    private $stripeSecretKey;

    public function __construct(){
        if ($_SESSION['usersoid555564546Tuydy6']['role'] === 'nanny'){
            $this->Abort403();
        }
        if (empty($_SESSION['usersoid555564546Tuydy6']['nom'])){
            $this->redirect('AccueilParents');
        }
        $stripeSecretKey = 'sk_test_51OwMif09DtyGbK6ycHhkTKgCPAO515duxG1UKJrqh2LsFvv8lHgs6O0wEb9uYeqzOmwud0EJd4niO0aYSUymwNwa00G059e4qg';
        $this->stripeSecretKey = $stripeSecretKey;
        Stripe::setApiKey($this->stripeSecretKey);

    }

    public function payment()
    {
        $YOUR_DOMAIN = 'http://localhost:2323';

        $checkoutSession = CheckoutSession::create([
            'line_items' => [[

                'price' => 'price_1OzEwW09DtyGbK6yX5P1oQLl',
                'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => $YOUR_DOMAIN . '/success',
            'cancel_url' => $YOUR_DOMAIN . '/cancel',
        ]);
        header("HTTP/1.1 303 See Other");
        header("Location: " . $checkoutSession->url);

        $this->render('app.payer.payer',array(
            'checkoutsession' => $checkoutSession
        ),'parents');
    }

    public function index()
    {
        $this->render('app.payer.checkout',array(),'parents');
    }

    public function success()
    {
        $this->render('app.payer.sucess',array(),'parents');
    }

}

