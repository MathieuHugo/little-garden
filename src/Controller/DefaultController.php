<?php

namespace App\Controller;

use Core\Kernel\AbstractController;

/**
 *
 */
class DefaultController extends BaseController
{
    public function index()
    {
        $title = 'Little <span>Garden</span>';

        $presentation = 'Bienvenue sur Little Garden, votre passerelle privilégiée pour connecter parents et crèches, créant un jardin partagé où grandissent les liens et fleurissent les opportunités pour vos enfants.';

        $button = '<button class="accueil_button">
  Commencer
  <div class="icon">
    <svg
      height="24"
      width="24"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M0 0h24v24H0z" fill="none"></path>
      <path
        d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z"
        fill="currentColor"
      ></path>
    </svg>
  </div>
</button>
';

        //$this->dump($message);
        $this->render('app.default.frontpage',array(
            'title' => $title,
            'presentation' => $presentation,
            'button' => $button,
        ));
    }

    public function mention() {

        $this->render('app.default.mention');
    }

    public function cgu() {
        $this->render('app.default.cgu');
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }

    public function Page403()
    {
        $this->render('app.default.403');
    }
}
