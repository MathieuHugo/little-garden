<?php
namespace App\Controller;

use App\Model\ProfilenfantsModel;
use App\Service\Form;
use App\Service\Validation;

class ProfilenfantsController extends BaseController{

    public function __construct(){
        if ($_SESSION['usersoid555564546Tuydy6']['role'] === 'nanny'){
            $this->Abort403();
        }
        if (empty($_SESSION['usersoid555564546Tuydy6']['nom'])){
            $this->redirect('AccueilParents');
        }
    }

    public function index(){

        $enfants=ProfilenfantsModel::findByIdFalse($_SESSION['usersoid555564546Tuydy6']['id'],'id_user');
        $this->render('app.parent.profilenfants.index',array(
            'enfants'=>$enfants,
        ),'parents');
    }

    public function add(){
        $errors=[];
        if(!empty($_POST['submitted'])){
            $post=$this->cleanXss($_POST);
            $v=new Validation();
            $errors['nom']=$v->textValid($post['nom'],'nom',3,150 );
            $errors['prenom']=$v->textValid($post['prenom'],'nom',3,150 );
            $errors['naissance']=$v->dateNaissanceValid($post['naissance']);
//            $errors['allergie']=$v->textValid($post['allergie'],'allergie',1,500);
//            $errors['alimentation']=$v->textValid($post['alimentation'],'alimentation',1,500);
            if($v->IsValid($errors)){
                ProfilenfantsModel::insert($post,$_SESSION['usersoid555564546Tuydy6']['id']);
                $this->redirect('MesEnfants');
            }
        }
        $form=new Form($errors);
        $this->render('app.parent.profilenfants.add',array(
            'form'=>$form,
        ),'parents');
    }

    public function single($id){
        $enfant=ProfilenfantsModel::findById($id);
        if(empty($enfant)){
            $this->Abort404();
        }
        $this->render('app.parent.profilenfants.single',array(
            'enfant'=>$enfant,
        ),'parents');
    }

    public function modif($id){
        $errors=[];
        $enfant=ProfilenfantsModel::findById($id);
        if(empty($enfant)){
            $this->Abort404();
        }
        if(!empty($_POST['submitted'])){
            $post=$this->cleanXss($_POST);
            $v=new Validation();
            $errors['nom']=$v->textValid($post['nom'],'nom',3,150 );
            $errors['prenom']=$v->textValid($post['prenom'],'nom',3,150 );
            $errors['naissance']=$v->dateNaissanceValid($post['naissance']);
            $errors['allergie']=$v->textValid($post['allergie'],'allergie',1,500);
            $errors['alimentation']=$v->textValid($post['alimentation'],'alimentation',1,500);
            if($v->IsValid($errors)){
                ProfilenfantsModel::update($post,$id);
                $this->redirect('MesEnfants');
            }
        }
        $form=new Form($errors);
        $this->render('app.parent.profilenfants.modif',array(
            'form'=>$form,
            'enfant'=>$enfant,
        ),'parents');
    }

    public function delete($id){
        $enfant=ProfilenfantsModel::findById($id);
        if(empty($enfant)){
            $this->Abort404();
        }
        ProfilenfantsModel::delete($id);
        $this->redirect('MesEnfants');
    }
}