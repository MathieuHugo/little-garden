<?php
namespace App\Controller;

use App\Model\FacturationModel;
use App\Model\ReserveModel;

class AccueilparentsController extends BaseController{

    public function __construct(){
        if ($_SESSION['usersoid555564546Tuydy6']['role'] === 'nanny'){
            $this->Abort403();
        }
    }

    public function accueil(){
        $infoReserves = ReserveModel::infoReservationByIdUser($_SESSION['usersoid555564546Tuydy6']['id']);
        $this->render('app.parent.accueilparents.accueil',array(
            'infoReserves'=>$infoReserves,
        ),'parents');
    }

}