<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ProfilenfantsModel extends AbstractModel{
    protected static $table = 'lg_enfant';
    protected $id;
    protected $id_user;
    protected $nom;
    protected $prenom;
    protected $naissance;
    protected $allergie;
    protected $alimentation;
    protected $cdv;
    protected $cm;
    protected $created_at;
    protected $modified_at;
    public function getModifiedAt()
    {
        return $this->modified_at;
    }
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    public function getCm()
    {
        return $this->cm;
    }
    public function getCdv()
    {
        return $this->cdv;
    }
    public function getAlimentation()
    {
        return $this->alimentation;
    }
    public function getAllergie()
    {
        return $this->allergie;
    }
    public function getNaissance()
    {
        return $this->naissance;
    }
    public function getPrenom()
    {
        return $this->prenom;
    }
    public function getNom()
    {
        return $this->nom;
    }
    public function getIdUser()
    {
        return $this->id_user;
    }
    public function getId()
    {
        return $this->id;
    }

    public static function insert($post,$id_user){
        App::getDatabase()->prepareInsert("INSERT INTO ".self::$table." ( id_user, nom, prenom, naissance, allergie, alimentation, created_at) VALUES (?,?,?,?,?,?,NOW())",array($id_user,$post['nom'],$post['prenom'],$post['naissance'],$post['allergie'],$post['alimentation']));
    }

    public static function update($post,$id){
        App::getDatabase()->prepareInsert("UPDATE ".self::getTable()." SET nom = ?, prenom = ?, naissance = ?, allergie = ?, alimentation = ?, modified_at = NOW() WHERE id = ?", [$post['nom'], $post['prenom'], $post['naissance'],$post['allergie'],$post['alimentation'],$id]);
    }
    public static function findByIdFalse($id,$columId = 'id')
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE ".$columId." = ?",[$id],get_called_class());
    }
}