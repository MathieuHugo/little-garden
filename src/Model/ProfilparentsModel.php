<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ProfilparentsModel extends AbstractModel{
    protected static $table = 'lg_user';
    protected $id;
    protected $nom;
    protected $prenom;
    protected $adresse;
    protected $code_postale;
    protected $ville;
    protected $telephone;
    protected $email;
    protected $password;
    protected $token;
    protected $role;
    protected $created_at;
    protected $modified_at;

    // REQUÊTES

    public static function insertUser($post, $password, $token) {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (role,email,password,token,created_at) VALUES (?,?,?,?,NOW())",
            [$post['role'],$post['email'], $password, $token] // Enlever nom prénom
        );
    }

    public static function ubdateUser($post,$session) {
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET nom = ?,prenom = ?,adresse = ?,code_postale = ?,ville = ?,telephone = ?,modified_at = NOW() WHERE id= ?",
            [$post['nom'],$post['prenom'],$post['adresse'],$post['code_postal'],$post['ville'],$post['tel'],$session['usersoid555564546Tuydy6']['id']]
        );
    }

    public static function findByColumnAll($column,$value)
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE ".$column." = ?",[$value],get_called_class());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @return mixed
     */
    public function getCodePostale()
    {
        return $this->code_postale;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

}