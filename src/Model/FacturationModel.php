<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class FacturationModel extends AbstractModel{
    protected static $table = 'lg_facturation';
    protected $id;
    protected $id_user;
    protected $id_pro;
    protected $id_reservation;
    protected $prix_totale;
    protected $created_at;
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    public function getPrixTotale()
    {
        return $this->prix_totale;
    }
    public function getIdReservation()
    {
        return $this->id_reservation;
    }
    public function getIdPro()
    {
        return $this->id_pro;
    }
    public function getIdUser()
    {
        return $this->id_user;
    }
    public function getId()
    {
        return $this->id;
    }

    public static function insert($id_user,$id_pro,$id_res,$prix_totale){
        App::getDatabase()->prepareInsert("INSERT INTO ". self::$table ." (id_user,id_pro,id_reservation,prix_totale,created_at) VALUES (?,?,?,?,NOW())",array($id_user,$id_pro,$id_res,$prix_totale));
    }
    public static function findLastId(){
        return App::getDatabase()->query("SELECT id FROM ".self::$table." WHERE id= LAST_INSERT_ID()",get_called_class());
    }

}