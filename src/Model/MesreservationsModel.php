<?php
namespace App\Model;

use Core\Kernel\AbstractModel;

class MesreservationsModel extends AbstractModel{
    protected static $table = 'lg_facturation';
    protected $id;
    protected $id_user;
    protected $id_pro;
    protected $id_reservation;
    protected $prix_totale;
    protected $created_at;
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    public function getPrixTotale()
    {
        return $this->prix_totale;
    }
    public function getIdReservation()
    {
        return $this->id_reservation;
    }
    public function getIdPro()
    {
        return $this->id_pro;
    }
    public function getIdUser()
    {
        return $this->id_user;
    }
    public function getId()
    {
        return $this->id;
    }
}