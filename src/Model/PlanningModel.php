<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class PlanningModel extends AbstractModel
{
    protected static $table = 'lg_planning';
    protected $id;
    protected $id_pro;
    protected $lundi_start;
    protected $lundi_end;
    protected $mardi_start;
    protected $mardi_end;
    protected $mercredi_start;
    protected $mercredi_end;
    protected $jeudi_start;
    protected $jeudi_end;
    protected $vendredi_start;
    protected $vendredi_end;
    protected $samedi_start;
    protected $samedi_end;

    public function getId()
    {
        return $this->id;
    }
    public function getIdPro()
    {
        return $this->id_pro;
    }
    public function getLundiStart()
    {
        return $this->lundi_start;
    }
    public function getLundiEnd()
    {
        return $this->lundi_end;
    }
    public function getMardiStart()
    {
        return $this->mardi_start;
    }
    public function getMardiEnd()
    {
        return $this->mardi_end;
    }
    public function getMercrediStart()
    {
        return $this->mercredi_start;
    }
    public function getMercrediEnd()
    {
        return $this->mercredi_end;
    }
    public function getJeudiStart()
    {
        return $this->jeudi_start;
    }
    public function getJeudiEnd()
    {
        return $this->jeudi_end;
    }
    public function getVendrediStart()
    {
        return $this->vendredi_start;
    }
    public function getVendrediEnd()
    {
        return $this->vendredi_end;
    }
    public function getSamediStart()
    {
        return $this->samedi_start;
    }
    public function getSamediEnd()
    {
        return $this->samedi_end;
    }

    public static function insert($id_pro,$post){
        App::getDatabase()->prepareInsert("INSERT INTO ".self::$table." (id_pro,lundi_start,lundi_end,mardi_start,mardi_end,mercredi_start,mercredi_end,jeudi_start,jeudi_end,vendredi_start,vendredi_end,samedi_start,samedi_end) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",[$id_pro,$post['lundi_start'],$post['lundi_end'],$post['mardi_start'],$post['mardi_end'],$post['mercredi_start'],$post['mercredi_end'],$post['jeudi_start'],$post['jeudi_end'],$post['vendredi_start'],$post['vendredi_end'],$post['samedi_start'],$post['samedi_end']]);
    }

    public static function update($id,$post){
        App::getDatabase()->prepareInsert("UPDATE ".self::$table." SET lundi_start = ?, lundi_end = ?, mardi_start = ?, mardi_end = ?, mercredi_start = ?, mercredi_end = ?,
        jeudi_start = ?, jeudi_end = ?, vendredi_start = ?, vendredi_end = ?, samedi_start = ?, samedi_end = ? WHERE id_pro = ?",[$post['lundi_start'],$post['lundi_end'],$post['mardi_start'],$post['mardi_end'],$post['mercredi_start'],$post['mercredi_end'],$post['jeudi_start'],$post['jeudi_end'],$post['vendredi_start'],$post['vendredi_end'],$post['samedi_start'],$post['samedi_end'],$id]);
    }
    public static function findAllByIdPro($id_pro){
        return App::getDatabase()->prepare("SELECT * FROM ".self::$table." WHERE id_pro = ?",[$id_pro],get_called_class(),true);
    }
    public static function findIdByIdPro($id_pro){
        return App::getDatabase()->prepare("SELECT id FROM ".self::$table." WHERE id_pro = ?",[$id_pro],get_called_class(),true);
    }

}