<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;
use function PHPUnit\Framework\returnArgument;

class ProCompetenceModel extends AbstractModel
{
    protected static $table = 'lg_pro_competences';
    protected $id;
    protected $id_pro;
    protected $id_competence;

    public static function insert($post,$session) {
        foreach ($post['competence'] as $item) {
            App::getDatabase()->prepareInsert(
                "INSERT INTO " . self::$table . " (id_pro, id_competence) VALUES (?,?)",
                [$session,$item]
            );
        }
    }

    public static function findCompetenceByIdArray($id,$columId = 'id_pro')
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE ".$columId." = ?",[$id],get_called_class());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @return mixed
     */
    public function getIdCompetence()
    {
        return $this->id_competence;
    }

}