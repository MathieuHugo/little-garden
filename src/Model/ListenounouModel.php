<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ListenounouModel extends AbstractModel{
    protected static $table = 'lg_pro';
    protected $id;
    protected $id_user;
    protected $siren;
    protected $siret;
    protected $adresse;
    protected $code_postale;
    protected $ville;
    protected $max_enfant;
    protected $tarif;
    protected $statut;
    protected $image;
    protected $pdf;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @return mixed
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * @return mixed
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @return mixed
     */
    public function getCodePostale()
    {
        return $this->code_postale;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @return mixed
     */
    public function getMaxEnfant()
    {
        return $this->max_enfant;
    }

    /**
     * @return mixed
     */
    public function getTarif()
    {
        return $this->tarif;
    }

    /**
     * @return mixed
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return mixed
     */
    public function getPdf()
    {
        return $this->pdf;
    }
    public static function getUserByPro() {
        return App::getDatabase()->query(
            "SELECT lg_p .id, lg_u.id as id_user, lg_u.nom, lg_u.prenom, lg_u.role, lg_p.image, lg_p.ville
            FROM " . self::$table . " AS lg_p 
            LEFT JOIN lg_user AS lg_u ON lg_p.id_user = lg_u.id
            WHERE lg_p.statut = 'actif'"
            ,get_called_class());
    }

    public static function getUserByProVille($search)
    {
        return App::getDatabase()->prepare(
            "SELECT lg_p .id, lg_u.id as id_user, lg_u.nom, lg_u.prenom, lg_u.role, lg_p.image, lg_p.ville
            FROM " . self::$table . " AS lg_p 
            LEFT JOIN lg_user AS lg_u ON lg_p.id_user = lg_u.id
            WHERE lg_p.ville = ? AND lg_p.statut = 'actif'",[$search]
            ,get_called_class());
    }

    public static function getListAndPro()
    {
        return App::getDatabase()->query(
            "SELECT lg_p.id, lg_u.nom, lg_u.prenom, lg_u.email, lg_p.statut
            FROM " . self::$table . " AS lg_p 
            LEFT JOIN lg_user AS lg_u ON lg_p.id_user = lg_u.id",
            get_called_class()
        );
    }

}