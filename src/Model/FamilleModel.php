<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class FamilleModel extends AbstractModel{
    protected static $table = 'lg_accompagnant';
    protected $id;
    protected $id_enfant;
    protected $nom;
    protected $prenom;
    protected $telephone;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdEnfant()
    {
        return $this->id_enfant;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table ." (nom, prenom, telephone) VALUES (?,?,?)",
            [$post['nom'], $post['prenom'], $post['telephone']]
        );
    }

    public static function update($post, $id){
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . "SET nom = ?, prenom = ?, telephone = ? WHERE id = ?",
            [$post['nom'], $post['prenom'], $post['telephone'], $id]
        );
    }

//    public static function delete($post, $id){
//        App::getDatabase()->prepareInsert(
//            "DELETE "
//        );
//    }
}