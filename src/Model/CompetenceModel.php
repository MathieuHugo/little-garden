<?php

namespace App\Model;

use Core\Kernel\AbstractModel;

class CompetenceModel extends AbstractModel {
    protected static $table = 'lg_competences';
    protected $id;
    protected $competence;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCompetence()
    {
        return $this->competence;
    }
}