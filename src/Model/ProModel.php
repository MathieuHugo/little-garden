<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ProModel extends AbstractModel{
    protected static $table = 'lg_pro';
    protected $id;
    protected $id_user;
    protected $siren;
    protected $siret;
    protected $adresse;
    protected $code_postale;
    protected $ville;
    protected $max_enfant;
    protected $tarif;
    protected $statut;
    protected $image;
    protected $pdf;


    public static function insert($post,$session,$nameImg,$namePdf) {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (id_user,siren,siret,adresse,code_postale,ville,max_enfant,tarif,statut,image,pdf) 
            VALUES (?,?,?,?,?,?,?,?,?,?,?)",
            [$session['usersoid555564546Tuydy6']['id'],$post['siren'],$post['siret'],$post['adresse'],$post['code_postal'],$post['ville'],$post['max_enfant'],$post['tarif'],'inactif',$nameImg,$namePdf]
        );
    }

    public static function selectPlanningByJoinById($id_pro){
        return App::getDatabase()->prepare('
        SELECT pro.max_enfant,pla.lundi_start, pla.lundi_end,pla.mardi_start,pla.mardi_end,pla.mercredi_start,pla.mercredi_end,pla.jeudi_start,pla.jeudi_end,pla.vendredi_start,pla.vendredi_end,pla.samedi_start,pla.samedi_end 
        FROM lg_pro AS pro
        LEFT JOIN lg_planning AS pla
        ON pro.id = pla.id_pro 
        WHERE pro.id = ?',[$id_pro],get_called_class(),true);
    }

    public static function findIdByIdUser($id_user)
    {
        return App::getDatabase()->prepare("SELECT id FROM " . self::$table . " WHERE id_user = ?", [$id_user], get_called_class(), true);
    }
    public static function updateProStatut($post, $id)
    {
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET statut = ? WHERE id = ?",
            [$post['statut'], $id]
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @return mixed
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * @return mixed
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @return mixed
     */
    public function getCodePostale()
    {
        return $this->code_postale;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @return mixed
     */
    public function getMaxEnfant()
    {
        return $this->max_enfant;
    }

    /**
     * @return mixed
     */
    public function getTarif()
    {
        return $this->tarif;
    }

    /**
     * @return mixed
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return mixed
     */
    public function getPdf()
    {
        return $this->pdf;
    }
}