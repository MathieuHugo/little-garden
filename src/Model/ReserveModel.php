<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ReserveModel extends AbstractModel{

    protected static $table = 'lg_reservation';
    protected $id;
    protected $id_user;
    protected $id_pro;
    protected $date_start;
    protected $nbre_heure;
    protected $created_at;
    protected $modified_at;

    public function getId()
    {
        return $this->id;
    }
    public function getIdUser()
    {
        return $this->id_user;
    }
    public function getIdPro()
    {
        return $this->id_pro;
    }
    public function getDateStart()
    {
        return $this->date_start;
    }
    public function getNbreHeure()
    {
        return $this->nbre_heure;
    }
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    public static function insert($post,$id_user,$id_pro){
        App::getDatabase()->prepareInsert("INSERT INTO ".self::$table." ( id_user, id_enfant, id_pro, date_start, nbre_heure,created_at) VALUES (?,?,?,?,?,NOW())",array($id_user,$post['enfants'],$id_pro,$post['heure'],$post['nbreheures']));
    }

    public static function update($post,$id){
        App::getDatabase()->prepareInsert("UPDATE ".self::$table." SET id_enfant=? ,date_start = ?, nbre_heure = ?, modified_at=NOW() WHERE id = ?",[$post['enfants'],$post['heure'],$post['nbreheures'],$id]);
    }

    public static function findAllById($id_pro){
        return App::getDatabase()->prepare("SELECT * FROM ".self::$table." WHERE id_pro= ?",[$id_pro],get_called_class());
    }

    public static function findLastId(){
        return App::getDatabase()->query("SELECT id FROM ".self::$table." WHERE id= LAST_INSERT_ID()",get_called_class());
    }

    public static function findIdProByJoin($id){
        return App::getDatabase()->prepare("
        SELECT lg_pro.id FROM lg_reservation
        LEFT JOIN lg_pro
        ON lg_reservation.id_pro = lg_pro.id
        WHERE lg_reservation.id = ?
        ",[$id],get_called_class(),true);
    }

    public static function infoReservation($id){
        return App::getDatabase()->prepare("
        SELECT lg_e.prenom AS prenom_enfant, lg_e.nom AS nom_enfant, lg_upro.nom,lg_upro.prenom,lg_upro.telephone,lg_upro.adresse,lg_upro.code_postale,lg_upro.ville,lg_pro.id AS id_pro ,lg_pro.tarif,lg_res.id AS id_res,lg_res.date_start,lg_res.nbre_heure 
        FROM lg_reservation AS lg_res 
        LEFT JOIN lg_enfant AS lg_e ON lg_res.id_enfant = lg_e.id
        LEFT JOIN lg_pro ON lg_res.id_pro = lg_pro.id 
        LEFT JOIN lg_user AS lg_upro ON lg_pro.id_user = lg_upro.id 
        WHERE lg_res.id = ?",[$id],get_called_class(),true);
    }

    public static function infoReservationByIdpro($id_pro){
        return App::getDatabase()->prepare("
        SELECT lg_f.prix_totale, lg_e.prenom AS prenom_enfant, lg_e.nom AS nom_enfant, lg_u.nom,lg_u.prenom,lg_u.telephone,lg_e.naissance,lg_e.allergie,lg_e.alimentation, lg_u.ville,lg_pro.id AS id_pro ,lg_pro.tarif,lg_res.id AS id_res,lg_res.date_start,lg_res.nbre_heure 
        FROM lg_reservation AS lg_res 
        LEFT JOIN lg_enfant AS lg_e ON lg_res.id_enfant = lg_e.id
        LEFT JOIN lg_pro ON lg_res.id_pro = lg_pro.id 
        LEFT JOIN lg_user AS lg_u ON lg_res.id_user = lg_u.id 
        LEFT JOIN lg_facturation AS lg_f ON lg_res.id = lg_f.id_reservation
        WHERE lg_res.id_pro = ?
        ORDER BY lg_res.date_start ASC",[$id_pro],get_called_class());
    }

    public static function infoReservationByIdUser($id_user)
    {
        return App::getDatabase()->prepare("
        SELECT lg_f.prix_totale, lg_e.id AS id_enfant, lg_e.prenom AS prenom_enfant, lg_e.nom AS nom_enfant, lg_upro.nom,lg_upro.prenom,lg_upro.telephone,lg_upro.adresse,lg_upro.code_postale,lg_upro.ville,lg_pro.id AS id_pro ,lg_pro.tarif,lg_res.id AS id_res,lg_res.date_start,lg_res.nbre_heure 
        FROM lg_reservation AS lg_res 
        LEFT JOIN lg_enfant AS lg_e ON lg_res.id_enfant = lg_e.id
        LEFT JOIN lg_pro ON lg_res.id_pro = lg_pro.id 
        LEFT JOIN lg_user AS lg_upro ON lg_pro.id_user = lg_upro.id 
        LEFT JOIN lg_facturation AS lg_f ON lg_res.id = lg_f.id_reservation
        WHERE lg_res.id_user = ?
        ORDER BY lg_res.date_start ASC", [$id_user], get_called_class());
    }
}
