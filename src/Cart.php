<?php

namespace App;

class Cart
{
    public function getProducts(): array
    {
        return [
            [
                'name' => 'Peluche',
                'price' => 1499
            ],
            [
                'name' => 'Poumpon',
                'price' => 2099
            ]
        ];
    }

    public function getTotal(): int
    {

        return array_reduce($this->getProducts(), function (int $acc, array $product) {
            return $acc + $product['price'];
        }, 0);
    }
}
